//
//  HomeVC.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 14/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class HomeVC: SuperViewController {
    
    @IBOutlet weak var tableView: UITableViewX!
    var presenter: HomePresenter!
    override func setupUI() {
        presenter = HomePresenter(view: self)
        setupTableView()
    }
    
    override func fetchData() {
        presenter.load_news()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "\(NewsCell.self)", bundle: nil), forCellReuseIdentifier: "\(NewsCell.self)")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? NewInternalVC {
            vc.news = sender as? News
        }
        
    }
}

extension HomeVC: HomeView {
    func error_message(msg: String) {
        InfoAlert(title: "", message: msg)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    func view_did_load() {
        tableView.reloadData()
    }
    
}
extension HomeVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let hv = Bundle.main.loadNibNamed("HomeHeaderView", owner: nil, options: nil)?.first as? HomeHeaderView
        hv?.frame.size.height = 60
        switch section {
        case 0:
            hv?.nameLabel.text = "اخر الاخبار"
        default:
            hv?.nameLabel.text = ""
        }
        return hv
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "\(NewsCell.self)", for: indexPath) as! NewsCell
            cell.setupUI(news: presenter.get_news(index: indexPath.row)!)
            cell.didPressReadMore = { [weak self] in
                guard let self = self else { return }
                self.performSegue(withIdentifier: Constants.showNewsInternalSegue, sender: self.presenter.get_news(index: indexPath.row))
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return presenter.news_count
        default:
            return 0
        }

    }
    
}
