//
//  SideMenuDataSource.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 26/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxCocoa

protocol MenuView: AnyObject {
    func loadMenuItems()
}
class SideMenuPresenter: NSObject {

    let menuDataSource = BehaviorRelay<[MenuItem]>(value: [MenuItem(identifier: 1, name: "Home"), MenuItem(identifier: 2, name: "Game Week"), MenuItem(identifier: 3, name: "Premier Table"),MenuItem(identifier: 4, name: "Table Arrangement"), MenuItem(identifier: 9, name: "Videos"), MenuItem(identifier: 10, name: "Socities"),  MenuItem(identifier: 11, name: "Events")])
    
    //,MenuItem(identifier: 5, name: "Statistics"), MenuItem(identifier: 6, name: "Point Expectations"), MenuItem(identifier: 7, name: "Comparsations"), MenuItem(identifier: 8, name: "Articales"), MenuItem(identifier: 12, name: "Computations")
    
    private weak var view: MenuView?
    
    init(view: MenuView) {
        self.view = view
        super.init()
    }
    
    func prepareDataSource() {
        view?.loadMenuItems()
    }
    
    
}

struct MenuItem {
    var identifier: Int
    var name: String
}





