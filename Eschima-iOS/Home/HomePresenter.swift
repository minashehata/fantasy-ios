//
//  HomePresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 22/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

protocol HomeView: AnyObject, SpinnerView {
    func view_did_load()
    func error_message(msg: String)
}


final class HomePresenter: NSObject {
    
    private weak var view: HomeView?
    
    private let gameWeekInteractor = GameWeekInteractor()
    private var homeDataSource = HomeData()
    init(view: HomeView) {
        self.view = view
        super.init()
    }
    
    func load_news() {
        view?.ShowSpinner()
        gameWeekInteractor.load_news { [weak self] news, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let news = news {
                self.homeDataSource.news = news
                self.view?.view_did_load()
            }
            else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    func get_news(index: Int) -> News? {
        return homeDataSource.news?[index]
    }
    var news_count: Int {
        return homeDataSource.news?.count ?? 0
    }

    
}

struct HomeData {
    var news: [News]?
    
}
