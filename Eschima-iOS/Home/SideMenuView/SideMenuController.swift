//
//  SideMenuView.swift
//  Vedetta
//
//  Created by Mina Shehata on 11/25/19.
//  Copyright © 2019 Vedetta LLC. All rights reserved.
//

import RxSwift

class SideMenuController: SuperViewController {
 
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    var presenter: SideMenuPresenter!
    fileprivate let bag = DisposeBag()
    
    @IBOutlet weak var loginLogoutUser: UIButtonX!
    @IBOutlet weak var SettingsPageButton: UIButton!
    
    
    override func setupUI() {
       
        presenter = SideMenuPresenter(view: self)
        loginLogoutUser.setTitle(UserInteractor.getUser() == nil ? "Login".Localize : "Logout".Localize, for: .normal)
        tableView.register(UINib(nibName: Constants.sideMenuCell, bundle: nil), forCellReuseIdentifier: Constants.sideMenuCell)
        tableView.tableHeaderView = headerView
        bindLoginLogoutButtonPressed()
        bindSettingsPageButton()
    }
    
    
    override func fetchData() {
        bindTableViewWithDataSource()
        presenter.prepareDataSource()
    }
    private func bindSettingsPageButton() {
        SettingsPageButton.rx.tap
            .do(onNext: { 
                Transition.goToStoryboard(with: "Settings")
            })
            .subscribe()
            .disposed(by: bag)
    }
    private func bindTableViewWithDataSource() {
        presenter.menuDataSource.bind(to: tableView.rx.items) { [weak self] tableView, index, item in
            let indexPath = IndexPath(row: index, section: 0)
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.sideMenuCell, for: indexPath) as! SideMenuCell
            cell.menuButton.setTitle(item.name.Localize, for: .normal)
            _ = cell.menuButton.rx.tap
                .subscribe(onNext: { [weak self] in
                    self?.didSelectMenuButton(item: item)
                })
            return cell
        }.disposed(by: bag)
    }
    private func didSelectMenuButton(item: MenuItem) {
        switch item.identifier {
        case 1:
            Transition.goToStoryboard(with: "Home")
        case 2:
            Transition.goToStoryboard(with: "GameWeek")
        case 3:
            Transition.goToStoryboard(with: "PremierTable")
        case 4:
            Transition.goToStoryboard(with: "TableArrangement")
        case 9:
            Transition.goToStoryboard(with: "VideosAndCategory")
        case 10:
            Transition.goToStoryboard(with: "Community")
        case 11:
            Transition.goToStoryboard(with: "Events")
        default:
            break
        }
    }
    
    private func bindLoginLogoutButtonPressed() {
        loginLogoutUser.rx.tap
            .do(onNext: {
                if UserInteractor.getUser() != nil {
                    UserInteractor.removeUser()
                } else {
                    Transition.goToStoryboard(with: "Loading")
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
    
}

extension SideMenuController: MenuView {
    
    func loadMenuItems() {
        tableView.reloadData()
    }
}
