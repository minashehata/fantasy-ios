//
//  CommunityVC.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import RxSwift

class CommunityVC: SuperViewController {
   

    var presenter: CommunityPresenter!
    
    @IBOutlet weak var tableView: UITableViewX!
    private let bag = DisposeBag()
    
    override func setupUI() {
        presenter = CommunityPresenter(view: self)
        setupTableViewUI()
    }
    
    override func fetchData() {
        presenter.load_posts()
    }
    override func viewAppear() {
        reload_post_data()
    }

    private func setupTableViewUI() {
        tableView.register(UINib(nibName: "\(PostCell.self)", bundle: nil), forCellReuseIdentifier: "\(PostCell.self)")
        tableView.delegate = self
        tableView.dataSource = self
    }

}
extension CommunityVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "\(PostCell.self)", for: indexPath) as!
            PostCell
        cell.setupUI(post: self.presenter?.getPost(index: indexPath.row))
        cell.didTapLikePost = { [weak self]  in
            guard let self = self else { return }
            self.presenter?.likeButtonPressed(post: self.presenter?.getPost(index: indexPath.row))
        }
        cell.didTapCommentPost = { [weak self] in
            guard let self = self else { return }
            self.openCommentPage(post: self.presenter.getPost(index: indexPath.row))
        }
        cell.didTapNoPost = { [weak self] in
            guard let self = self else { return }
            self.presenter.answerPost(index: indexPath.row, answer: false)
        }
        cell.didTapYesPost = { [weak self] in
            guard let self = self else { return }
            self.presenter.answerPost(index: indexPath.row, answer: true)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.posts_count
    }
    
    private func openCommentPage(post: Post) {
        let controller = CommentsVC()
        controller.presenter = CommentsPresenter(commentView: controller, post: post)
        navigationController?.pushViewController(controller, animated: true)
    }
    
}
extension CommunityVC: CommunityView {
    func reload_post_data() {
        tableView.reloadData()
    }
    
    func error_message(msg: String) {
        InfoAlert(title: "", message: msg)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
        
    func show_success_message(msg: String) {
        SuccessAlert(title: "", message: msg)
    }
}
