//
//  CommunityInteractor.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

struct CommunityInteractor {
    
    func getCommunities(page: Int = 1, completion: @escaping (_ posts: [Post]?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.communities, parameters: ["page": page], method: .get) { data, success in
            if (!success) {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let posts: [Post] = data["data"].map { return Post(json: $0.1) }
                completion(posts, nil)
            }
        }
    }
    
    func likeOrDisLikePost(isLiked: Bool, postid: Int, completion: @escaping (_ message: String?, _ error: String?) -> ()) {
        let url = isLiked ? URLs.dislike : URLs.like
        NetworkLoader.shared.request(url: url + "?postId=\(postid)", method: .post) { data, success in
            if !success {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                completion(data["msg"].string, nil)
            }
        }
    }
    
    func answerPoll(postId: Int, answer: Bool, completion: @escaping (_ msg: String?, _ yes: Int?, _ no: Int?, _ error: String?) -> ()) {
        let url = URLs.answerPoll + "?postId=\(postId)&answer=\(answer)"
        NetworkLoader.shared.request(url: url, method: .post) { data, success in
            if !success {
                completion(nil, nil, nil, data?["msg"].string)
                return
            }
            if let data = data {
                completion(data["msg"].string, data["data"]["yesPercentage"].int, data["data"]["noPercentage"].int, nil)
            }
        }
    }
    
}
