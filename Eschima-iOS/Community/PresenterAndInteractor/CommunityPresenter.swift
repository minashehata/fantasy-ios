//
//  CommunityPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol CommunityView: AnyObject, SpinnerView {
    func error_message(msg: String)
    func reload_post_data()
    func show_success_message(msg: String)
}

final class CommunityPresenter: NSObject {
    
    private weak var view: CommunityView?
    
    private let interactor = CommunityInteractor()
    var dataSource = [Post]()
    var page: Int = 1
    
    init(view: CommunityView) {
        self.view = view
        super.init()
    }
    
    var posts_count: Int {
        return dataSource.count
    }
    
    func load_posts(page: Int = 1) {
        view?.ShowSpinner()
        interactor.getCommunities(page: page) { [weak self] posts, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let posts = posts {
                if posts.count > 0 {
                    self.page += 1
                    self.load_posts(page: self.page)
                    self.dataSource.append(contentsOf: posts)
                    self.view?.reload_post_data()
                    return
                }
                
            }else{
                self.view?.error_message(msg: error ?? "")
            }

        }
    }
    func getPost(index: Int) -> Post {
        return dataSource[index]
    }
    // like
     func likeButtonPressed(post: Post?) {
        post!.likesCount! += (post!.isLiked ?? false) ? -1 : 1
        self.doLikePost(isLiked: post!.isLiked!, post: post)
        post!.isLiked! = (post!.isLiked ?? false) ? false : true
        self.view?.reload_post_data()
    }
    
    private func doLikePost(isLiked: Bool, post: Post?) {
        view?.ShowSpinner()
        interactor.likeOrDisLikePost(isLiked: isLiked, postid: post?.id ?? 0) { [weak self] message, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let message = message {
                self.view?.show_success_message(msg: message)
            } else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    
    func answerPost(index: Int, answer: Bool) {
        guard let id = dataSource[index].id else { return }
        view?.ShowSpinner()
        interactor.answerPoll(postId: id, answer: answer) { [weak self] message, yes, no, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let message = message {
                self.dataSource[index].yesPercentage = yes
                self.dataSource[index].noPercentage = no
                self.view?.show_success_message(msg: message)
                self.view?.reload_post_data()
            } else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    
}
