//
//  CommentInteractor.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/5/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import Foundation


struct CommentInteractor {
    
    func addComment(postId: Int, msg: String, completion: @escaping (_ message: String?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.comment + "?postId=\(postId)&comment=\(msg)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: .post) { (data, success) in
            if !success {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                completion(data["msg"].string, nil)
            }
        }
    }
}
