//
//  CommentsVC+TableExtension.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/5/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import UIKit

extension CommentsVC {
    @objc func sendButtonPressed(_ sender: UIButton) {
        guard let text = inputTextView.text, !text.isEmpty else {
            return
        }
        presenter.send_comment(msg: text)
        inputTextView.text = nil
        view.endEditing(true)
    }
}

extension CommentsVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}

extension CommentsVC: UITableViewDataSource {
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if presenter.comments_count == 0 {
            footerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 200)
        } else {
            footerView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        }
        return presenter.comments_count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "\(PostCell.self)", for: indexPath) as! PostCell
            cell.setupUI(post: presenter.post)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "\(CommentCell.self)", for: indexPath) as! CommentCell
            cell.setupComment(comment: presenter.get_comment(index: indexPath.row - 1)!)
            return cell
        }
    }
    
    
    
    
}


