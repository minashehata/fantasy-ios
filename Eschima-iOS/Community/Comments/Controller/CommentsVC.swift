//
//  CommentsVC.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/4/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class CommentsVC: SuperViewController {
    
    var presenter: CommentsPresenter!
    
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.dataSource = self
        tv.backgroundColor = .white
        tv.separatorStyle = .none
        tv.tableFooterView = footerView
        tv.register(UINib(nibName: "\(PostCell.self)", bundle: nil), forCellReuseIdentifier: "\(PostCell.self)")
        tv.register(CommentCell.self, forCellReuseIdentifier: "\(CommentCell.self)")
        return tv
    }()
    
    lazy var footerView: UIViewX = {
        let v = UIViewX(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 300))
        v.backgroundColor = .white
        let label = UILabel()
        label.textAlignment = .center
        label.text = "write your comment".Localize
        label.numberOfLines = 0
        label.textColor = .black
        v.addSubview(label)
        label.fillSuperView()
        return v
    }()
    lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "share_icon"), for: .normal)
        button.tintColor = .FDarkGreen
        button.addTarget(self, action: #selector(sendButtonPressed(_:)), for: .touchUpInside)
        return button
    }()
    lazy var inputTextView: IQTextView = {
        let tv = IQTextView()
        tv.backgroundColor = .white
        tv.placeholder = "Type something...".Localize
        tv.isScrollEnabled = false
        tv.layer.cornerRadius = 5
        tv.layer.masksToBounds = true
        tv.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        return tv
    }()

    let holderTextView: UIViewX = {
        let htv = UIViewX()
        htv.backgroundColor = UIColor.lightGray
        htv.defaultShadow = true
        return htv
    }()
    
    // MARK:- update outlets
    lazy var update_comment_view: UIView = {
        let uv = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 60))
        uv.backgroundColor = .clear
        uv.addSubview(updateTextView)
        updateTextView.anchor(top: uv.topAnchor, leading: uv.leadingAnchor, bottom: uv.bottomAnchor, trailing: uv.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5))
        return uv
    }()
    
    let updateTextView: IQTextView = {
        let tv = IQTextView()
        tv.backgroundColor = .white
        tv.placeholder = "Type something...".Localize
        tv.layer.cornerRadius = 5
        tv.layer.masksToBounds = true
        tv.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        tv.layer.borderColor = UIColor.darkGray.cgColor
        tv.layer.borderWidth = 2
        return tv
    }()
    
   
    override func setupUI() {
        let customView = UIViewX()
        view.backgroundColor = .white
        customView.firstColor = .FRedColor
        customView.secondColor = .FDarkGreen
        customView.thirdColor = .FCyanColor
        customView.horizontalGradient = true
        customView.horizontalGradientStartPoint = CGPoint(x: 0.5, y: 0)
        customView.horizontalGradientEndPoint = CGPoint(x: 0.5, y: 0.8)

        view.addSubview(customView)
        customView.fillSuperView()
        setupCommentsTableView()
        
    }
    override func fetchData() {
        presenter.viewDidLoad()

    }
    
    
    private func setupCommentsTableView() {
        view.addSubview(tableView)
        view.addSubview(holderTextView)
        if #available(iOS 11.0, *) {
            tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: holderTextView.topAnchor, trailing: view.trailingAnchor)
        } else {
            // Fallback on earlier versions
        }
        
        if #available(iOS 11.0, *) {
            holderTextView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: 0, right: 0))
        } else {
            // Fallback on earlier versions
        }
        
        holderTextView.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        holderTextView.addSubview(inputTextView)
        inputTextView.anchor(top: holderTextView.topAnchor, leading: holderTextView.leadingAnchor, bottom: holderTextView.bottomAnchor, trailing: nil, padding: .init(top: 5, left: 5, bottom: 5, right: 0))
        //
        holderTextView.addSubview(sendButton)
        sendButton.anchor(top: holderTextView.topAnchor, leading: inputTextView.trailingAnchor, bottom: holderTextView.bottomAnchor, trailing: holderTextView.trailingAnchor, padding: .init(top: 5, left: 5, bottom: 5, right: 5), size: CGSize(width: 40, height: 0))
    }
    
}
