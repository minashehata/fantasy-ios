//
//  CommentsVC+PresenterExtension.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/5/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import Foundation

extension CommentsVC: CommentsView {
    func load_comments_success() {
        tableView.reloadData()
    }
    func updateUIWithPost(post: Post?) {
        tableView.reloadData()
    }
    func load_comments_error(error: String) {
        ErrorAlert(title: "Error", message: error)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    func send_comment_error(error: String) {
        ErrorAlert(title: "", message: error)
    }
    func send_comment_success(message: String) {
        SuccessAlert(title: "", message: message)
    }
    
    func deleteCommentSuccessfully(message: String, index: Int) {
        SuccessAlert(title: "", message: message)
    }
}
