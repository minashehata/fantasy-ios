//
//  CommentsPresenter.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/5/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import UIKit

protocol CommentsView: AnyObject, SpinnerView {
    func load_comments_success()
    func updateUIWithPost(post: Post?)
    func load_comments_error(error: String)
    func send_comment_success(message: String)
    func send_comment_error(error: String)
    func deleteCommentSuccessfully(message: String, index: Int)
    
}

class CommentsPresenter: NSObject {

    private final weak var commentsView: CommentsView?
    private final let commentInteractor = CommentInteractor()
    private final var comments: [Comment]?
    
    final var post: Post?
    init(commentView: CommentsView, post: Post) {
        self.commentsView = commentView
        self.post = post
        self.comments = post.comments
        super.init()
    }
    
    
    func viewDidLoad() {
        commentsView?.updateUIWithPost(post: post)
    }
    
    // MARK:- send comment to vote
    func send_comment(msg: String) {
        guard let postid = post?.id else { return }
        commentsView?.ShowSpinner()
        commentInteractor.addComment(postId: postid, msg: msg) { [weak self] (message, status) in
            guard let self = self else { return }
            self.commentsView?.HideSpinner()
            if let message = message {
                let comment = Comment()
                comment.comment = msg
                self.comments?.append(comment)
                self.post?.comments?.append(comment)
                self.post!.commentsCount! += 1
                self.commentsView?.load_comments_success()
                self.commentsView?.send_comment_success(message: message)
            } else {
                self.commentsView?.send_comment_error(error: message ?? "")
            }
        }
    }
    
    func get_comment(index: Int) -> Comment? {
        guard let comment = comments?[index] else { return nil }
        return comment
    }
    
    var comments_count: Int {
        return comments?.count ?? 0
    }
}
