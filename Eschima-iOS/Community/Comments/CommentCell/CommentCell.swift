//
//  CommentCell.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/4/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
   
    lazy var containerView: UIViewX = {
        let cv = UIViewX()
        cv.backgroundColor = UIColor.white
        cv.cornerRadius = 10
        cv.clipsToBounds = true
        cv.borderColor = UIColor.FDarkGreen
        cv.borderWidth = 1
        return cv
    }()
    
    
    let voteCommentLabel: UILabelX = {
        let label = UILabelX()
        label.textColor = .black
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 10, weight: .regular)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCommentUI()
    }
    
    private final func setupCommentUI() {
        selectionStyle = .none
        backgroundColor = .white
        
        addSubview(containerView)
        containerView.anchor(top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor, padding: .init(top: 6, left: 6, bottom: 6, right: 6))
        containerView.addSubview(voteCommentLabel)
        
        voteCommentLabel.anchor(top: containerView.topAnchor, leading: containerView.leadingAnchor, bottom: containerView.bottomAnchor, trailing: containerView.trailingAnchor, padding: .init(top: 4, left: 8, bottom: 8, right: 8))
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupComment(comment: Comment) {
        voteCommentLabel.text = comment.comment
    }
}
