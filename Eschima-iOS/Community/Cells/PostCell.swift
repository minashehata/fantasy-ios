//
//  PostCell.swift
//  Eschima-iOS
//
//  Created by Mina on 15/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {

    
    @IBOutlet weak var profileIconImageView: UIImageViewX!
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var postCaptionLabel: UILabel!
    
    @IBOutlet weak var postDescLabel: UILabel!
    @IBOutlet weak var postImageView: UIImageViewX!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!

    @IBOutlet weak var yesButton: UIButtonX!
    @IBOutlet weak var noButton: UIButtonX!
    @IBOutlet weak var voteStackView: UIStackView!
    
    
    var didTapLikePost: (() -> ())?
    var didTapCommentPost: (() -> ())?

    var didTapYesPost: (() -> ())?
    var didTapNoPost: (() -> ())?
    
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var sharesCountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    
    }

    
    func setupUI(post: Post?) {
        postTitleLabel.text = "FANTASY FANS CLUB"
        let dates = post?.date?.split(separator: "T")
        postCaptionLabel.text = "@FFC" + String(dates?.first ?? "")
        postDescLabel.text = post?.title
        NetworkLoader.shared.downloadImageToImageView(imageView: &postImageView, url: post?.imagePath ?? "")
        likesCountLabel.text = "\(post?.likesCount ?? 0)"
        commentsCountLabel.text = "\(post?.commentsCount ?? 0)"
        sharesCountLabel.text = "\(post?.sharesCount ?? 0)"
        likeButton.setImage(post!.isLiked! ? UIImage(named: "liked_icon") : UIImage(named: "like_icon"), for: .normal)
        
        if let isPoll = post?.isPoll {
            yesButton.isHidden = !isPoll
            noButton.isHidden = !isPoll
            voteStackView.isHidden = !isPoll
            yesButton.setTitle("Yes".Localize + " \(post?.yesPercentage ?? 0) %", for: .normal)
            noButton.setTitle("No".Localize + " \(post?.noPercentage ?? 0) %", for: .normal)
        }
        
    }
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        if let didTapLikePost = didTapLikePost {
            didTapLikePost()
        }
    }
    @IBAction func commentButtonPressed(_ sender: UIButton) {
        if let didTapCommentPost = didTapCommentPost {
            didTapCommentPost()
        }
    }
    
    @IBAction func yesButtonPressed(_ sender: UIButtonX) {
        if let didTapYesPost = didTapYesPost {
            didTapYesPost()
        }
        
    }
    
    @IBAction func noButtonPressed(_ sender: UIButtonX) {
        if let didTapNoPost = didTapNoPost {
            didTapNoPost()
        }
    }
    
}
