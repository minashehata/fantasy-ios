//
//  EventsInteractor.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

struct EventsInteractor {
    
    func load_events(page: Int = 1, completion: @escaping (_ events: [Event]?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.events, parameters: ["page": page], method: .get) { data, success in
            if !success {
                completion(nil, data?["msg"].string)
            }
            if let data = data {
                let events: [Event] = data["data"].map { return Event(json: $0.1) }
                completion(events, nil)
            }
        }
    }
    
}
