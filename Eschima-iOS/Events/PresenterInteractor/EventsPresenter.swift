//
//  EventsPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxCocoa
import RxSwift

protocol EventsView: AnyObject, SpinnerView {
    func load_events()
    func error_message(msg: String)
}

final class EventsPresenter: NSObject {
    
    
    private weak var view: EventsView?
    private let interactor = EventsInteractor()
    
    let dataSource = BehaviorRelay<[Event]>(value: [])
    var tempEvents = [Event]()
    private var page = 1
    init(view: EventsView) {
        self.view = view
        super.init()
    }
    
    func load_events(page: Int = 1) {
        view?.ShowSpinner()
        interactor.load_events(page: page) { [weak self] events, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let events = events {
                if events.count > 0 {
                    self.page += 1
                    self.load_events(page: self.page)
                    self.tempEvents.append(contentsOf: events)
                    return
                }
                self.dataSource.accept(self.tempEvents)
            }else{
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    
    
    
}
