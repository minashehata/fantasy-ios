//
//  EventCell.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }

    func setupUI(event: Event) {
        titleLabel.text = event.title
        let dates = event.date?.split(separator: "T")
        
        dateLabel.text = String(dates?.first ?? "")
        locationLabel.text = event.location
    }
    
}
