//
//  EventsViewController.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import RxSwift

class EventsViewController: SuperViewController {

    
    @IBOutlet weak var tableView: UITableViewX!
    var presenter: EventsPresenter!
    private let bag = DisposeBag()
    override func setupUI() {
        presenter = EventsPresenter(view: self)
        tableView.register(UINib(nibName: "\(EventCell.self)", bundle: nil), forCellReuseIdentifier: "\(EventCell.self)")
        bindTableViewDataSource()
    }
    
    override func fetchData() {
        presenter.load_events()
    }

    private func bindTableViewDataSource() {
        presenter.dataSource
            .compactMap { $0.filter { $0.isActive == true } }
            .bind(to: tableView.rx.items) { tableView, index, event in
                let indexPath = IndexPath(row: index, section: 0)
                let cell = tableView.dequeueReusableCell(withIdentifier: "\(EventCell.self)", for: indexPath) as!
                    EventCell
                cell.setupUI(event: event)
                return cell
            }.disposed(by: bag)
    }
    
}

extension EventsViewController: EventsView {
    func load_events() {
        
    }
    
    func error_message(msg: String) {
        InfoAlert(title: "", message: msg)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    

    
    
}

