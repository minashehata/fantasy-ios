//
//  Router.swift
//  Medica-Egypt-iOS
//
//  Created by Mina Shehata on 1/14/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import UIKit
//import OneSignal
class Router {
    
    class func restartApp() {
        guard let window = UIApplication.shared.windows.first else { return }
        var vc: UIViewController?
        
        if let _ = UserInteractor.getUser() {
            vc = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as? TransparentNC
        } else {
            vc = UIStoryboard(name: "SignIn", bundle: nil).instantiateInitialViewController() as? TransparentNC
        }
        AppDelegate.changeLanguage(Lang: "ar")
        window.rootViewController = vc
        Transition.transition(with: window)
    }
    
    
    class func restartAppLangauage(with language: String) {
        AppDelegate.changeLanguage(Lang: language)
        guard let window = UIApplication.shared.windows.first else { return }
        window.rootViewController = UIStoryboard(name: "Home", bundle: nil).instantiateInitialViewController() as? TransparentNC
        Transition.transition(with: window)
    }
    
}
