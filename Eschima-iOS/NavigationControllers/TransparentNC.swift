//
//  TransparentNC.swift
//  Vedetta
//
//  Created by Mina Shehata on 11/20/19.
//  Copyright © 2019 Vedetta LLC. All rights reserved.
//

import UIKit
import SideMenu

class TransparentNC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        setupTransparentAppearance()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UINavigationController {
    func setupTransparentAppearance() {
        prepareAppFont("VEXA")
        navigationBar.shadowImage = UIImage()
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.tintColor = .white
        var font = UIFont(name: "VEXA", size: 30)
        font = UIFontMetrics(forTextStyle: .largeTitle).scaledFont(for: font ?? UIFont.systemFont(ofSize: 30))
        navigationBar.prefersLargeTitles = false
        let largeAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            .font : font
        ]
        let smallAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            .font : font
        ]
        navigationBar.largeTitleTextAttributes = largeAttributes
        navigationBar.titleTextAttributes = smallAttributes
        
    }
    
}

class SideMenuNavigationTransparent: SideMenuNavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareAppFont("VEXA")
        if let navCont = navigationController as? SideMenuNavigationTransparent {
            navCont.blurEffectStyle = .dark
            navCont.settings.animationOptions = .curveEaseInOut
        }
    }

}
