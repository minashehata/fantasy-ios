//
//  TransparentNC.swift
//  Vedetta
//
//  Created by Mina Shehata on 11/11/19.
//  Copyright © 2019 Vedetta LLC. All rights reserved.
//

import UIKit

class BrownNC: UINavigationController {
    

    public override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup() {
        /////// some setups
        prepareAppFont("VEXA")
        setupLisner()
        setupGradiantNavigationControllerAppearance()
    }


    
    //MARK:- Observers Or Lisner
    private func setupLisner() {
        NotificationCenter.default.addObserver(self, selector: #selector(networkChanged(notification:)), name: .StatusChanged, object: nil)
    }
    
    
    //MARK:- network lisners
    @objc private func networkChanged(notification: Notification) {
        if let status = notification.userInfo!["connected"] as? Bool
        {
            if status {
                NetworkLoader.shared.resume()
            }
            else {
                InfoAlert(title: "Connection".Localize, message: "Network offline".Localize)
            }
        }
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeLisner()
    }
    
    private func removeLisner() {
        NotificationCenter.default.removeObserver(self, name: .StatusChanged, object: nil)
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}


extension UINavigationController {
    func setupGradiantNavigationControllerAppearance() {
        let components = GradiantComponent(colors: [UIColor.FDarkGreen.cgColor, UIColor.FDarkGreen.cgColor], locations: [0.3, 1], startPoint:  CGPoint(x: -0.3, y: -0.1), endPoint: CGPoint(x: 0.8, y: 0.8))
        navigationBar.applyNavigationBarGradiant(components: components)
        navigationBar.tintColor = .white
        navigationBar.shadowImage = UIImage()
        var font = UIFont.systemFont(ofSize: 16, weight: .light)
        font = UIFontMetrics(forTextStyle: .title1).scaledFont(for: font)
        navigationBar.prefersLargeTitles = false
        let largeAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            .font : font
        ]
        let smallAttributes =  [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            .font : font
        ]
        navigationBar.largeTitleTextAttributes = largeAttributes
        navigationBar.titleTextAttributes = smallAttributes
    }
    
    
}






