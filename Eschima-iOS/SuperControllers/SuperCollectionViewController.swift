//
//  BaseCollectionViewController.swift
//  Chainz
//
//  Created by Youssef mostafa  on 5/14/19.
//  Copyright © 2019 Youssef mostafa . All rights reserved.
//

import UIKit

class SuperCollectionViewController: UICollectionViewController {

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let keyboardManager = DPKeyboardManager()

    // MARK: - DATASOURCE
    
    lazy var statusBarView: UIViewX = {
        let sv = UIViewX()
        sv.alpha = 0
        sv.setupDefaultShadow()
        sv.backgroundColor = .FRedColor//status_gradiant_horizontal()
        return sv
    }()
    // MARK: - INITS
    
    
    // MARK: - LIFE CYCLE METHODS
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareAppFont("VEXA")
        self.keyboardManager.loadKeyboardEvents(self)
        setupUI()
        prepareAttributes()
        setupSubViews()
        setupConstraints()
        registerCells()
        prepareDataSources()
        assignDelegates()
        setupNavigationBar()
//        addObservers()
        fetchData()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardManager.enableKeybaordEvents()
        navigationController?.navigationBar.transform = .identity
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardManager.disableKeyboardEvents()
        viewDisappear()
    }
    
    func viewAppear() {
        addTopStatusBarBackgroundColor()
    }
    
    func viewDisappear() {
        removeTopStatusBarBacgroundColor()
    }
    
    // MARK: - METHODS
    
    func prepareAttributes() {}
    func setupSubViews() {}
    func setupConstraints() {}
    func setupUI() {}
    func prepareDataSources() {}
    func fetchData() {}
    func assignDelegates() {}
    func registerCells() {}
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {}
    
    @objc func keyboardWillHide(_ notification: Notification) {}
    
    func setupNavigationBar() {
    }
    
    private final func addTopStatusBarBackgroundColor() {
        if collectionView.contentOffset.y == 0 {
            statusBarView.isHidden = true
        }
        guard let window = UIApplication.shared.keyWindow else { return }
        window.addSubview(statusBarView)
        statusBarView.anchor(top: window.topAnchor, leading: window.leadingAnchor, bottom: nil, trailing: window.trailingAnchor, size: CGSize(width: 0, height: hasTopNotch ? 30 : 20))
    }
    private final func removeTopStatusBarBacgroundColor() {
        statusBarView.removeFromSuperview()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            statusBarView.isHidden = true
        } else {
            statusBarView.isHidden = false
        }
        let safeAreaTop: CGFloat = UIApplication.shared.windows.filter { $0.isKeyWindow }.first?.safeAreaInsets.top ?? 0
        
        let magicalSafeAreaTop: CGFloat = safeAreaTop + (navigationController?.navigationBar.frame.height ?? 0)
        
        let offset = scrollView.contentOffset.y + magicalSafeAreaTop
        
        //        let alpha: CGFloat = 1 - ((scrollView.contentOffset.y + magicalSafeAreaTop) / magicalSafeAreaTop)
        statusBarView.alpha = CGFloat(min(100, offset / 100))
        
        //        print("offset is ===? \(offset)")
        
        //        print("alpha is ===? \(alpha)")
        
        navigationController?.navigationBar.transform = CGAffineTransform(translationX: 0, y: min(0, -offset))
    }
    
    
    // MARK: - DeInits
    
    deinit { NotificationCenter.default.removeObserver(self) }
    
}
