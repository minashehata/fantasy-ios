//
//  BaseViewController.swift
//  Chainz
//
//  Created by Youssef mostafa  on 5/6/19.
//  Copyright © 2019 Youssef mostafa . All rights reserved.
//

import UIKit

class SuperViewController: UIViewController {
    
    let keyboardManager = DPKeyboardManager()

    // MARK: - LIFE CYCLE METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        self.keyboardManager.loadKeyboardEvents(self)
        prepareDataSources()
        setupUI()
        prepareAttributes()
        setupSubViews()
        setupConstraints()
        registerCells()
        assignDelegates()
        setupNavigationBar()
//        addObservers()
        fetchData()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.keyboardManager.enableKeybaordEvents()

        navigationController?.navigationBar.transform = .identity
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.keyboardManager.disableKeyboardEvents()
        viewDisappear()
    }
    
    func viewAppear() { }
    
    func viewDisappear() { }
    
    // MARK: - METHODS
    
    func prepareAttributes() {}
    func setupSubViews() {}
    func setupConstraints() {}
    func setupUI() {}
    func prepareDataSources() {}
    func fetchData() {}
    func assignDelegates() {}
    func registerCells() {}
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {}
    
    @objc func keyboardWillHide(_ notification: Notification) {}
    
    func setupNavigationBar() {
    }
    
    // MARK: - DeInits
    
    deinit { NotificationCenter.default.removeObserver(self) }
}
