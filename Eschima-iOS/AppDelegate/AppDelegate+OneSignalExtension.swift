//
//  AppDelegate+OneSignalExtension.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 6/23/19.
//  Copyright © 2019 Vootly. All rights reserved.
//
//
import UIKit
//import Firebase
//import FirebaseMessaging
//import UserNotifications
//
extension AppDelegate {//: UNUserNotificationCenterDelegate, MessagingDelegate {

    // notification stuff.........====================================>>>>>>>>>>
//    func setupNotification(application: UIApplication) {
//        UNUserNotificationCenter.current().delegate = self
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (grant, error) in
//            if let error = error {
//                print("error in request authorize, \(error.localizedDescription)")
//                return
//            }
//            if grant {
//                DispatchQueue.main.async {
//                    application.registerForRemoteNotifications()
//                }
//            }
//        }
//        application.applicationIconBadgeNumber = 0
//        Messaging.messaging().delegate = self
//        AppDelegate.get_new_token()
//    }
//
//    class func delete_token() {
//        InstanceID.instanceID().deleteID { (error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//        }
//    }
//
//    class func get_new_token() {
//        InstanceID.instanceID().instanceID { (result, error) in
//            if let error = error {
//                print("Error fetching remote instance ID: \(error)")
//            } else if let result = result {
//                UserDefaults.standard.set(result.token, forKey: "fcm_token")
//                UserDefaults.standard.synchronize()
//                print("Remote instance ID token: \(result.token)")
//            }
//        }
//    }
//
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//        UserDefaults.standard.set(fcmToken, forKey: "fcm_token")
//        UserDefaults.standard.synchronize()
//    }
//
//    func openNotification(with launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
//        if let lo = launchOptions, let notification = lo[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable : Any] {
//
//            guard let data = (notification["item"] as! String).data(using: .utf8) else { return }
//
//            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [])
//
//            if let jsonObject = jsonObject as? [String: Any] {
//                print(jsonObject)
//                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
//                }
//            }
//        }
//    }

}

//MARK:- Notification Delegate
//extension AppDelegate {
//    //MARK:- notification delegate
//    func setupDeviceToken(device_token: Data) -> String {
//        let bytes = [UInt8](device_token)
//        var token = ""
//        for byte in bytes {
//            token += String(format: "%02x", byte)
//        }
//        return token
//    }
//
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        print("++++++>   \(setupDeviceToken(device_token: deviceToken))")
//    }
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print(error.localizedDescription)
//    }
//
//   
//
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        print(userInfo)
//
//        completionHandler(UIBackgroundFetchResult.newData)
//    }
//}
//
////MARK:- Notification Delegate
//extension AppDelegate {
//   
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        completionHandler([.sound, .alert, .badge])
//    }
//    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        let request = response.notification.request.content.userInfo as! [String: Any]
////        guard let data = (request["item"] as? String)?.data(using: .utf8) else { return }
//
////        let jsonObject = try? JSONSerialization.jsonObject(with: data, options: [])
//        
////            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
////                NotificationCenter.default.post(name: .reservationNotification, object: nil, userInfo: jsonObject)
////            }
//        
//        completionHandler()
//    }
//
//    
//}
