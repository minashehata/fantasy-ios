//
//  AppDelegate+Extension.swift
//  Medica-Egypt-iOS
//
//  Created by Mina Shehata on 1/13/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import IQKeyboardManagerSwift
//import FBSDKCoreKit
//import GoogleSignIn
//import TwitterKit
//import GooglePlaces

extension AppDelegate {
    
    // MARK:- font
    func setupAppFontAppearance() {
        for family in UIFont.familyNames {
            print("=========>>>>>>>>>>>>>>>>>>>>>>")
            print(family)
            for font in UIFont.fontNames(forFamilyName: family) {
                print(font)
            }
            print("=========>>>>>>>>>>>>>>>>>>>>>>")
        }
    }

    
    // MARK:- Localization........
    class func changeLanguage(Lang: String) {
        Language.setAppLanguage(lang: Lang)
        UINavigationBar.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UITextViewX.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UITextFieldX.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UIButtonX.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UILabelX.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UIStackView.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UIView.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        UICollectionView.appearance().semanticContentAttribute = (Lang == "ar") ? .forceRightToLeft : .forceLeftToRight
        Localizer.doExchange() // call this every time you need to change Localize
    }
    
    //MARK:- IQKeyboard manager
    func setupIQKeyboardManagerAppearance() {
//        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
        IQKeyboardManager.shared.toolbarTintColor = .FDarkGreen
//        IQKeyboardManager.shared.shouldPlayInputClicks = true
    }

//    func setupGoogleSign() {
//        GIDSignIn.sharedInstance().clientID = Constants.clientId
////        GIDSignIn.sharedInstance().delegate = self
//    }
//
//    //MARK:- Facebook delegate functions.....
//    func setupFacebookSignin(with application: UIApplication, didFinishLaunchingWithOptions: [UIApplication.LaunchOptionsKey : Any]?) {
//        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: didFinishLaunchingWithOptions)
//    }

//    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//
//        guard let googleHandler = GIDSignIn.sharedInstance()?.handle(url) else { return false }
//
//        let facebookHandler = ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
//
//        if googleHandler {
//            return googleHandler
//        } else {
//          return facebookHandler
//        }
//
//    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        //MARK:- TODO:- here open app from outside......
        return true
    }

    //MARK:- Setup google maps
    func setupGoogleMaps() {
        //MARK:- google map api
//        GMSs.provideAPIKey(Constants.googleMapAPI_Key)
//        GMSPlacesClient.provideAPIKey(Constants.googleMapAPI_Key)
    }

}
