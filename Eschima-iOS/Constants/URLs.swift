//
//  URLs.swift
//  Vootly-iOS
//
//  Created by Mina Shehata on 5/11/19.
//  Copyright © 2019 Vootly. All rights reserved.
//

import Foundation

struct URLs {
    static let baseURL = "http://ahmedslama-001-site23.itempurl.com/api/"
    static let imageURL = "http://ahmedslama-001-site23.itempurl.com/"
//    static let sdkURL = "https://api.footystats.org/"
    
    // signIn
    static let login = baseURL + "LogIn"
    static let register = baseURL + "SignUp"
    
    
    
    //services
    static let all_game_weeks = baseURL + "GetAllGameWeeks"
    static let table_arrangement = baseURL + "GetOrderTable"
    
    // videos and categories
    static let search_videos = baseURL + "GetVideosSearchByGameWeekAndCategoryAndTitle"
    static let videos_and_categories = baseURL + "GetVideosByGameWeekAndCategory"
    //
    static let events = baseURL + "GetEvents"
    //
    static let communities = baseURL + "GetPostsAndPolls"
    static let dislike = baseURL + "UnLike"
    static let like = baseURL + "Like"
    static let comment = baseURL + "AddComment"
    static let answerPoll = baseURL + "AnswerPoll"
    static let load_news = baseURL + "GetNewsByGameWeek"
    static let free_hits = baseURL + "GetFreeHitByGameWeek"
    static let wild_card = baseURL + "GetWildCardByGameWeek"
    static let captin_week = baseURL + "GetCaptainsByGameWeek"
    
    static let teams = baseURL + "GetAllTeams"
    static let expected_creation = baseURL + "GetLineUpsByGameWeekAndTeamName"
    
    static let settings = baseURL + "GetSettings"
    
}

