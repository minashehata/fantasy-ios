//
//  VideosInteractor.swift
//  Eschima-iOS
//
//  Created by Mina on 09/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxSwift

struct VideosInteractor {
    func loadVideosAndCategories(gameWeek: String, category: String, search: String, completion: @escaping (_ videos: [Video]?, _ error: String?) -> ()) {
        let url = search != "" ? "\(URLs.search_videos)?gameWeek=\(gameWeek)&category=\(category)&title=\(search)" : "\(URLs.videos_and_categories)?gameWeek=\(gameWeek)&category=\(category)"
        NetworkLoader.shared.request(url: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!, method: .get) { (data, success) in
            if !success {
                completion(nil, data?["msg"].string)
            }
            if let data = data {
                print(data)
                let weeks = data["data"].map { return Video(json: $0.1) }
                completion(weeks, nil)
            }
        }
    }
}
