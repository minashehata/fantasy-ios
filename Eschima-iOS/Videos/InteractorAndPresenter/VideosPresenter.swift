//
//  VideosPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 09/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxCocoa
import RxSwift

enum VideosCategoryType: Int {
    case fantacy_famous, week_analysis, fantacy_coffee, fantacy_stories
        
    var value:String  {
        switch self {
        case .fantacy_famous: return "مشاهير الفانتزي"
        case .week_analysis: return "تحليل الاسبوع"
        case .fantacy_coffee: return "قهوة الفانتزى"
        case .fantacy_stories: return "حكاوى الفانتزى"
        }
    }
}


protocol VideosView: AnyObject, SpinnerView {
    func load_data_success()
    func error_did_recieve(error: String)
    func openVideoWithPlayer(url: String)
}

class VideosPresenter: NSObject {
    
    private weak var view: VideosView?
    private let interactor = VideosInteractor()
    private let premierTableInteractor = PremierTableInteractor()
    private let disposeBag = DisposeBag()
    //
    var gameWeeks: [GameWeek]
    let dataSource: BehaviorRelay<[Video]> = .init(value: [])
    let category = BehaviorRelay<VideosCategoryType>(value: .fantacy_famous)
    let gameWeek = BehaviorRelay<GameWeek?>(value: nil)
    let searchText = BehaviorRelay<String>(value: "")
    let didTapOpenVideoButton = PublishRelay<String>()
    //
    init(view: VideosView) {
        self.view = view
        self.gameWeeks = []
        super.init()
    }
    
    
    func viewDidLoad() {
        bindGameWeekSelection()
        bindCategorySelection()
        bindSearchTextDidChange()
        bindDidTapOpenVideo()
        loadAllGameWeeks()
    }
    func loadAllGameWeeks() {
        view?.ShowSpinner()
        premierTableInteractor.loadGameWeeks { [weak self] (data, error) in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let data = data {
                self.gameWeeks = data.data ?? []
                self.gameWeek.accept(self.gameWeeks.first)
                self.view?.load_data_success()
            }else {
                // error here.
            }
        }
    }
    var gameWeeksCount: Int {
        return gameWeeks.count
    }
    func gameWeekName(index: Int) -> String {
        return gameWeeks[index].name ?? ""
    }
    func didSelectGameWeek(index: Int) {
        self.gameWeek.accept(gameWeeks[index])
    }
    
    //
    func choose_category(category: VideosCategoryType) {
        switch category {
        case .fantacy_stories:
            self.category.accept(.fantacy_stories)
        case .fantacy_famous:
            self.category.accept(.fantacy_famous)
        case .fantacy_coffee:
            self.category.accept(.fantacy_coffee)
        case .week_analysis:
            self.category.accept(.week_analysis)
        }
    }
    

    private func bindGameWeekSelection() {
        gameWeek
            .skip(1)
            .do(onNext: { [weak self] (gameweek) in
                guard let self = self else { return }
                self.load_videos(category: self.category.value.value, gameWeek: "\(gameweek?.id ?? 0)", text: self.searchText.value)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    private func bindSearchTextDidChange() {
        searchText
            .do(onNext: { [weak self] (text) in
                guard let self = self else { return }
                self.load_videos(category: self.category.value.value, gameWeek: "\(self.gameWeek.value?.id ?? 0)", text: text)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func bindCategorySelection() {
        category
            .skip(1)
            .do(onNext: { [weak self] (category) in
                guard let self = self else { return }
                self.load_videos(category: category.value, gameWeek: "\(self.gameWeek.value?.id ?? 0)", text: self.searchText.value)
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func load_videos(category: String, gameWeek: String, text: String) {
        view?.ShowSpinner()
        interactor.loadVideosAndCategories(gameWeek: gameWeek, category: category, search: text) { [weak self] videos, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let videos = videos {
                self.dataSource.accept(videos)
            } else {
                self.view?.error_did_recieve(error: error ?? "")
            }
        }
    }
    
    func bindDidTapOpenVideo() {
        didTapOpenVideoButton
            .subscribe(on: MainScheduler.asyncInstance)
            .do(onNext: { url in
                self.view?.openVideoWithPlayer(url: url)
            })
            .subscribe()
            .disposed(by: disposeBag)

    }
    
}
