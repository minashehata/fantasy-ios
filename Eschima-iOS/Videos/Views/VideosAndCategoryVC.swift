//
//  VideosAndCategoryVC.swift
//  Eschima-iOS
//
//  Created by Mina on 09/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import MKDropdownMenu
import RxSwift
import RxCocoa
import AVFoundation
import AVKit

class VideosAndCategoriesVC: SuperViewController {
    
    //
    var presenter: VideosPresenter!
    private let bag = DisposeBag()
    //

    @IBOutlet weak var videoTapsSegmentControll: UISegmentedControl!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var videosTableView: UITableViewX!
    @IBOutlet weak var videosHeaderTableView: UIView!

    @IBOutlet weak var gameWeekDropdown: MKDropdownMenu! {
        didSet {
            gameWeekDropdown.delegate = self
            gameWeekDropdown.dataSource = self
            setupDropdown(dropdown: gameWeekDropdown)
        }
    }
    override func prepareDataSources() {
        presenter = VideosPresenter(view: self)
    }

    override func setupUI() {
        videosTableView.register(UINib(nibName: "\(VideoCell.self)", bundle: nil), forCellReuseIdentifier: "\(VideoCell.self)")
        videosTableView.tableHeaderView = videosHeaderTableView
        bindTableViewDataSource()
        bindSearchTextFieldDidChange()
        bindSegmentControlDidChange()
    }
    
    override func fetchData() {
        presenter.viewDidLoad()
    }
    
    override func viewDisappear() {
        gameWeekDropdown.closeAllComponents(animated: true)
    }
    private func bindTableViewDataSource() {
        presenter.dataSource
            .bind(to: videosTableView.rx.items) { [weak self] tableView, index, video in
                guard let self = self else { return UITableViewCell() }
                let indexPath = IndexPath(row: index, section: 0)
                let cell = tableView.dequeueReusableCell(withIdentifier: "\(VideoCell.self)", for: indexPath) as!
                    VideoCell
                cell.setupUI(video: video)
                cell.playButton.rx.tap
                    .do(onNext: {
                        self.presenter.didTapOpenVideoButton.accept(video.videoUrl ?? "")
                    })
                    .subscribe()
                    .disposed(by: self.bag)
                return cell
            }.disposed(by: bag)
        
    }
    private func bindSearchTextFieldDidChange() {
        searchTextField.rx.text.orEmpty
            .filter { $0 != "" }
            .do(onNext: {[weak self] text in
                guard let self = self else { return }
                self.presenter.searchText.accept(text)
            })
            .subscribe()
            .disposed(by: bag)
        
    }
    private func bindSegmentControlDidChange() {
        videoTapsSegmentControll.rx.value
            .compactMap { VideosCategoryType(rawValue: $0)}
            .bind(to: presenter.category)
            .disposed(by: bag)
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? YoutubeVideoController {
            vc.url = sender as? String
        }
    }
}

extension VideosAndCategoriesVC: MKDropdownMenuDelegate, MKDropdownMenuDataSource {
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 1
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return presenter.gameWeeksCount
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForComponent component: Int) -> String? {
        return presenter.gameWeek.value?.name
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.gameWeekName(index: row)
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectGameWeek(index: row)
        dropdownMenu.reloadComponent(0)
        dropdownMenu.closeAllComponents(animated: true)
    }
    
}

extension VideosAndCategoriesVC: VideosView {
    func openVideoWithPlayer(url: String) {
        performSegue(withIdentifier: Constants.youtubeVideoControllerSegue, sender: url)
    }
    
    func load_data_success() {
        gameWeekDropdown.reloadComponent(0)
        gameWeekDropdown.closeAllComponents(animated: true)
    }
    
    func error_did_recieve(error: String) {
        InfoAlert(title: "", message: error)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    func HideSpinner() {
        StopLoading()
    }
}
