//
//  YoutubeVideoController.swift
//  Eschima-iOS
//
//  Created by Mina on 13/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import YouTubePlayer

class YoutubeVideoController: SuperViewController {

    @IBOutlet weak var videoView: YouTubePlayerView!
    var url: String?
    override func setupUI() {
        if let url = url, let myVideoURL = URL(string: url) {
            videoView.loadVideoURL(myVideoURL)
        }
    }
    
    
    
    
}
