//
//  VideoCell.swift
//  Eschima-iOS
//
//  Created by Mina on 11/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {

    @IBOutlet weak var videoImageView: UIImageViewX!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    func setupUI(video: Video) {
        titleLabel.text = video.title
        NetworkLoader.shared.downloadImageToImageView(imageView: &videoImageView, url: video.imagePath ?? "")
        descriptionLabel.text = video.desc
    }
    
    
    
}
