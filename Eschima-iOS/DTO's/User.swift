//
//  UserDTO.swift
//  Medica-Egypt-iOS
//
//  Created by Mina Shehata on 1/13/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import SwiftyJSON

enum UserType: String {
    case client = "client"
    case business = "business"
}

enum SignType: String {
    
    case signin
    case facebook
    case google
    
}

class GenericDataArray<T>: Decodable where T: Decodable  {
    var data: T?
    
    enum CodingKey {
        case data
    }
}

class User: NSObject, NSCoding {
    
    var id: Int?
    var name: String?
    var email: String?
    var phone: String?
    var teamId: Int?
    var userId: Int?
    var token: String?
    var teamName: String?
    var country: String?
    // business atributes

    var create_password: Bool?
    var unread_notifications_count: Int?
    var is_blocked: Bool?
    
    
    // for resign in with user
//    var sign_type: String
    var password: String?
    var social_auth_token: String?
    var socail_secret: String?
    var can_create_password: Bool?
    
    
    init?(with json: JSON) {
        
        self.id = json["id"].int
        self.name = json["name"].string
        self.email = json["email"].string
        self.phone = json["phone"].string
        self.teamId = json["teamId"].int
        self.userId = json["userId"].int
        self.token = json["token"].string
        self.teamName = json["teamName"].string
        self.country = json["country"].string
        
        //
        self.is_blocked = json["is_blocked"].bool
        self.create_password = json["create_password"].bool
        self.unread_notifications_count = json["unread_notifications_count"].int
        self.can_create_password = json["can_create_password"].bool
        //
        
        super.init()
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        teamId = aDecoder.decodeObject(forKey: "teamId") as? Int
        userId = aDecoder.decodeObject(forKey: "userId") as? Int
        
        country = aDecoder.decodeObject(forKey: "country") as? String

        token = aDecoder.decodeObject(forKey: "token") as? String
        
        is_blocked = aDecoder.decodeObject(forKey: "is_blocked") as? Bool
        
        unread_notifications_count = aDecoder.decodeObject(forKey: "unread_notifications_count") as? Int
        create_password = aDecoder.decodeObject(forKey: "create_password") as? Bool
        
        
        //
//        sign_type = aDecoder.decodeObject(forKey: "sign_type") as! String
        password = aDecoder.decodeObject(forKey: "password") as? String
        social_auth_token = aDecoder.decodeObject(forKey: "social_auth_token") as? String
        socail_secret = aDecoder.decodeObject(forKey: "socail_secret") as? String
        can_create_password = aDecoder.decodeObject(forKey: "can_create_password") as? Bool
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name ,forKey: "name")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(teamId, forKey: "teamId")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(country, forKey: "country")
        aCoder.encode(token, forKey: "token")
        aCoder.encode(is_blocked, forKey: "is_blocked")

        aCoder.encode(create_password, forKey: "create_password")
        aCoder.encode(unread_notifications_count, forKey: "unread_notifications_count")
        //
        aCoder.encode(can_create_password, forKey: "can_create_password")
//        aCoder.encode(sign_type, forKey: "sign_type")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(socail_secret, forKey: "socail_secret")
        aCoder.encode(social_auth_token, forKey: "social_auth_token")
    }
    
    
}

class UserBusiness: NSObject, NSCoding {
    var id: Int?
    
    var name: String?
    var desc: String?
    var location: String?
    var working_days: [String]?
    var working_hours_from: String?
    var working_hours_to: String?
    var currency: Currency?
    var facebook: String?
    var instagram: String?
    var logo: String?
    var cover: String?
    var city: City?
    var area: Area?
    var rate: Int?
    
    // not cashed
    
   
    init?(with json: JSON) {
        self.id = json["id"].int
        self.name = json["name"].string
        self.desc = json["description"].string
        self.location = json["location"].string
        self.cover = json["cover"].string
        let wdone = json["working_days"]["0"].string == nil ?  (json["working_days"].array?.first?.stringValue ?? "") : json["working_days"]["0"].stringValue
        let wdtwo = json["working_days"]["1"].string == nil ? (json["working_days"].array?.last?.string ?? "") : json["working_days"]["1"].stringValue
        self.working_days = [wdone ,wdtwo]
        if let hour_from = json["working_hours_from"].string {
            var hf_comp = hour_from.split(separator: ":")
            if hf_comp.count == 3 {
                hf_comp.removeLast()
                let newFormat = "\(String(hf_comp[0])):\(String(hf_comp[1]))"
                self.working_hours_from = newFormat
            } else {
                self.working_hours_from = json["working_hours_from"].string
            }
        }
        if let hour_to = json["working_hours_to"].string {
            var hf_comp = hour_to.split(separator: ":")
            if hf_comp.count == 3 {
                hf_comp.removeLast()
                let newFormat = "\(String(hf_comp[0])):\(String(hf_comp[1]))"
                self.working_hours_to = newFormat
            } else {
                self.working_hours_to = json["working_hours_to"].string
            }
        }
        self.currency = Currency(json: json["currency"]["data"])
        self.facebook = json["facebook"].string
        self.instagram = json["instagram"].string
        self.logo = json["logo"].string
        self.city = City(json: json["city"]["data"])
        self.area = Area(json: json["area"]["data"])
        self.rate = json["rate"].int
        //
        
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        desc = aDecoder.decodeObject(forKey: "desc") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        cover = aDecoder.decodeObject(forKey: "cover") as? String
        working_days = aDecoder.decodeObject(forKey: "working_days") as? [String]
        working_hours_from = aDecoder.decodeObject(forKey: "working_hours_from") as? String
        working_hours_to = aDecoder.decodeObject(forKey: "working_hours_to") as? String
        currency = aDecoder.decodeObject(forKey: "currency") as? Currency
        facebook = aDecoder.decodeObject(forKey: "facebook") as? String
        instagram = aDecoder.decodeObject(forKey: "instagram") as? String
        logo = aDecoder.decodeObject(forKey: "logo") as? String
        city = aDecoder.decodeObject(forKey: "city") as? City
        area = aDecoder.decodeObject(forKey: "area") as? Area
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id ,forKey: "id")
        aCoder.encode(name ,forKey: "name")
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(cover, forKey: "cover")
        aCoder.encode(working_days, forKey: "working_days")
        aCoder.encode(working_hours_from, forKey: "working_hours_from")
        aCoder.encode(working_hours_to, forKey: "working_hours_to")
        aCoder.encode(currency, forKey: "currency")
        aCoder.encode(facebook, forKey: "facebook")
        aCoder.encode(instagram, forKey: "instagram")
        aCoder.encode(logo, forKey: "logo")
        aCoder.encode(city, forKey: "city")
        aCoder.encode(area, forKey: "area")
    }
    
    
}

class City: NSObject, NSCoding {
    
    var id: Int?
    var name: String?
    var areas: [Area]?
    var image: String?
    var selected = false
    init(json: JSON) {
        self.id = json["id"].int
        self.name = json["name"].string
        self.areas = json["areas"]["data"].map { return Area(json: $0.1)}
        self.image = json["image"].string
        super.init()
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(areas, forKey: "areas")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        areas = aDecoder.decodeObject(forKey: "areas") as? [Area]
    }
    
    
}

class Area: NSObject, NSCoding {
    var id: Int?
    var name: String?
    var selected = false
    
    init(json: JSON) {
        self.id = json["id"].int
        self.name = json["name"].string
        super.init()
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
    }
}


class Day {
    var name: String?
    var abbreviation: String?
    var selected = false
    
    init(json: JSON) {
        self.name = json["name"].string
        self.abbreviation = json["abbreviation"].string
    }
    
    init(name: String, abbreviation: String) {
        self.abbreviation = abbreviation
        self.name = name
    }
    
}


class Currency: NSObject, NSCoding {
    var id: Int?
    var name: String?
    var abbreviation: String?
//    var selected = false
    
    init(json: JSON) {
        self.id = json["id"].int
        self.name = json["name"].string
        self.abbreviation = json["abbreviation"].string
        super.init()
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(abbreviation, forKey: "abbreviation")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        abbreviation = aDecoder.decodeObject(forKey: "abbreviation") as? String
    }
      
}
