//
//  GameWeek.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 31/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation
import RxSwift

class GameWeek: Decodable {
    var id: Int?
    var name: String?
    var deadline_time: String?
    var average_entry_score: Int?
    var highest_score: Int?
    var finished: Bool?
    var most_captained: Int?
    var most_vice_captained: Int?
    var fixtures: [Fixture]
    
    enum CodingKey {
        case id, name, deadline_time, average_entry_score, highest_score, finished, most_captained, most_vice_captained, fixtures
    }
}

class Fixture: Decodable {
    var id: Int?
    var code: Int?
    var event: Int?
    var finished: Bool?
    var kickoff_time: String?
    var team_h: Int?
    var team_h_score: Int?
    var teamH: Team?
    var team_a: Int?
    var teamA: Team?
    var team_a_score: Int?
    var team_h_difficulty: Int?
    var team_a_difficulty: Int?
    
    enum CodingKey {
        case id,
             code,
             event,
             finished,
             kickoff_time,
             team_h,
             team_h_score,
             teamH,
             team_a,
             teamA,
             team_a_score,
             team_h_difficulty,
             team_a_difficulty
    }
}

class Team: Decodable {
    var id: Int?
    var code: Int?
    var name: String?
    var logo: String?
    var short_name: String?
    var strength: Int?
    
    enum CodingKey {
        case id, code, name, logo, short_name, strength
        
    }
}
