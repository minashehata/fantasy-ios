//
//  Event.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import SwiftyJSON

struct Event {
    var id: Int?
    var title: String?
    var date: String?
    var location: String?
    var link: String?
    var desc: String?
    var isActive: Bool?
    
    init(json: JSON) {
        self.id = json["id"].int
        self.title = json["title"].string
        self.date = json["date"].string
        self.location = json["location"].string
        self.link = json["link"].string
        self.desc = json["description"].string
        self.isActive = json["isActive"].bool
    }
}
