//
//  Post.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import SwiftyJSON

class Post {
    var id: Int?
    var title: String?
    var date: String?
    var likesCount: Int?
    var sharesCount: Int?
    var commentsCount: Int?
    var isLiked: Bool?
    var isPoll: Bool?
    var isPollAnswered: Bool?
    var pollAnswer: Bool?
    var imagePath: String?
    
    var comments: [Comment]?
    var likes: [Like]?
    var yesPercentage: Int?
    var noPercentage: Int?
    
    init(json: JSON) {
        self.id = json["id"].int
        self.title = json["title"].string
        self.date = json["date"].string
        self.likesCount = json["likesCount"].int
        self.sharesCount = json["sharesCount"].int
        self.commentsCount = json["commentsCount"].int
        self.isLiked = json["isLiked"].bool
        self.isPoll = json["isPoll"].bool
        self.isPollAnswered = json["isPollAnswered"].bool
        self.imagePath = "\(URLs.imageURL)\(json["imagePath"].string ?? "")"
        self.comments = json["comments"].map { return Comment(json: $0.1) }
        self.yesPercentage = json["yesPercentage"].int
        self.noPercentage = json["noPercentage"].int
    }
    
}


class Comment {
    var id: Int?
    var postId: Int?
    var username: String?
    var userImage: String?
    var date: String?
    var comment: String?
    
    init() {}
    init(json: JSON) {
        self.id = json["id"].int
        self.postId = json["postId"].int
        self.username = json["username"].string
        self.userImage = URLs.imageURL + json["userImage"].stringValue
        self.date =  json["date"].string
        self.comment = json["comment"].string
    }
}

struct Like {
    
}
