//
//  News.swift
//  Eschima-iOS
//
//  Created by Mina on 17/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import SwiftyJSON

struct News {
    var id: Int?
    var title: String?
    var newsText: String?
    var gameWeek: String?
    var teams: String?
    var creationDate: String?
    var imagePath: String?
    init(json: JSON) {
        self.id = json["id"].int
        self.title = json["title"].string
        self.newsText = json["newsText"].string
        self.gameWeek = json["gameWeek"].string
        self.teams = json["teams"].string
        self.creationDate = json["creationDate"].string
        self.imagePath = URLs.imageURL + json["imagePath"].stringValue
    }
}
