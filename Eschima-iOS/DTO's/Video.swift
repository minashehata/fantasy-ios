//
//  Video.swift
//  Eschima-iOS
//
//  Created by Mina on 09/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import SwiftyJSON

struct Video {
    var id: Int?
    var category: String?
    var date: String?
    var imagePath: String?
    var videoUrl: String?
    var title: String?
    var desc: String?
    
    init(json: JSON) {
        self.id = json["id"].int
        self.category = json["category"].string
        self.date = json["date"].string
        self.imagePath = URLs.imageURL + json["imagePath"].stringValue
        self.videoUrl = json["videoUrl"].stringValue
        self.title = json["title"].string
        self.desc = json["description"].string
    }
   
    
}

