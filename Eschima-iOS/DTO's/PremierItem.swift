//
//  PremierItem.swift
//  Eschima-iOS
//
//  Created by Mina on 05/06/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

class PremierItem: Decodable {
    
    var order: Int?
    var points: Int?
    var played: Int?
    var won: Int?
    var drawn: Int?
    var lost: Int?
    var score: Int?
    var receive: Int?
    var goalDifference: Int?
    var team: Team?
    
    enum CodingKey {
        case order
        case points
        case played
        case won
        case drawn
        case lost
        case score
        case receive
        case goalDifference
        case team
    }
    
}
