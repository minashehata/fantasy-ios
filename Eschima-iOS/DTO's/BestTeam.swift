//
//  BestTeam.swift
//  Eschima-iOS
//
//  Created by Mina on 21/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import SwiftyJSON

struct BestTeam {
    var goalKeeper: [Player]?
    var defenders: [Player]?
    var midfielders: [Player]?
    var forwards: [Player]?
    init(json: JSON) {
        self.goalKeeper = json["goalKeeper"].map { return Player(json: $0.1) }
        self.defenders = json["defenders"].map { return Player(json: $0.1) }
        self.midfielders = json["midfielders"].map { return Player(json: $0.1) }
        self.forwards = json["forwards"].map { return Player(json: $0.1) }
    }
    
}

struct Player {
    var id: Int?
    var first_name: String?
    var second_name: String?
    var web_name: String?
    var playerTeam: String?
    var photo: String?
    
    init(json: JSON) {
        self.id = json["id"].int
        self.first_name = json["first_name"].string
        self.second_name = json["second_name"].string
        self.photo = json["photo"].string
        self.web_name = json["web_name"].string
        self.playerTeam = json["playerTeam"]["name"].string
    }
}


struct CaptinWeek {
    var player1: Player?
    var description1: String?
    var videoUrl1: String?
    
    var player2: Player?
    var description2: String?
    var videoUrl2: String?
    
    var player3: Player?
    var description3: String?
    var videoUrl3: String?
    
    var player4: Player?
    var description4: String?
    var videoUrl4: String?
    
    init(json: JSON) {
        self.player1 = Player(json: json["player1"])
        self.description1 = json["description1"].string
        self.videoUrl1 = json["videoUrl1"].string
        
        self.player2 = Player(json: json["player2"])
        self.description2 = json["description2"].string
        self.videoUrl2 = json["videoUrl2"].string
        
        self.player3 = Player(json: json["player3"])
        self.description3 = json["description3"].string
        self.videoUrl3 = json["videoUrl3"].string
        
        self.player4 = Player(json: json["player4"])
        self.description4 = json["description4"].string
        self.videoUrl4 = json["videoUrl4"].string
    }
}

struct ExpectedCreation {
    var id: Int?
    var gameWeek: String?
    var team: String?
    var imagePath: String?
    init(json: JSON) {
        self.id = json["id"].int
        self.gameWeek = json["gameWeek"].string
        self.team = json["team"].string
        self.imagePath = URLs.imageURL + json["imagePath"].stringValue
    }
}
