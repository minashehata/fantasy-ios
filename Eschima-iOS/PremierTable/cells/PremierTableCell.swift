//
//  PremierTableCell.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 31/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class PremierTableCell: UITableViewCell {

    @IBOutlet weak var teamANameLabel: UILabel!
    @IBOutlet weak var teamAImageView: CustomImageView!
    @IBOutlet weak var teamAScoreLabel: UILabel!
    
    @IBOutlet weak var matchDateLabel: UILabel!
    
    @IBOutlet weak var teamHScoreLabel: UILabel!
    @IBOutlet weak var teamHImageView: CustomImageView!
    @IBOutlet weak var teamHNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
