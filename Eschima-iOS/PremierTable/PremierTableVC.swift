//
//  PremierTableVC.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 31/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxCocoa
import MKDropdownMenu
import RxSwift

class PremierTableVC: SuperViewController {
    
    var presenter: PremierTablePresenter!
    fileprivate let bag = DisposeBag()
    @IBOutlet weak var tableView: UITableViewX!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var gameWeekDropdown: MKDropdownMenu! {
        didSet {
            gameWeekDropdown.delegate = self
            gameWeekDropdown.dataSource = self
            setupDropdown(dropdown: gameWeekDropdown)
        }
    }
    
    override func setupUI() {
        presenter = PremierTablePresenter(view: self)
        tableView.tableHeaderView = headerView
        tableView.register(UINib(nibName: Constants.premierTableCell, bundle: nil), forCellReuseIdentifier: Constants.premierTableCell)
    }
    
    override func fetchData() {
        bindTableViewWithDataSource()
        presenter.loadAllGameWeeks()
    }
    
    override func viewDisappear() {
        gameWeekDropdown.closeAllComponents(animated: true)
    }
    
    private func bindTableViewWithDataSource() {
        presenter.dataSource
            .bind(to: tableView.rx.items) { tableView, index, fixture in
                let indexPath = IndexPath(row: index, section: 0)
                guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.premierTableCell, for: indexPath) as? PremierTableCell else { return UITableViewCell() }
                cell.teamAImageView.loadProfileImageWithUrl(url: URL(string: fixture.teamA?.logo ?? "")!)
                cell.teamHImageView.loadProfileImageWithUrl(url: URL(string: fixture.teamH?.logo ?? "")!)
                cell.teamANameLabel.text = fixture.teamA?.short_name
                cell.teamHNameLabel.text = fixture.teamH?.short_name
                let dates = fixture.kickoff_time?.split(separator: "T")
                cell.matchDateLabel.text = String(dates?.first ?? "")
                cell.teamAScoreLabel.text = "\(fixture.team_a_score ?? 0)"
                cell.teamHScoreLabel.text = "\(fixture.team_h_score ?? 0)"
                return cell
        }.disposed(by: bag)
    }
}

extension PremierTableVC: MKDropdownMenuDelegate, MKDropdownMenuDataSource {
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 1
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return presenter.gameWeeksCount
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForComponent component: Int) -> String? {
        return presenter.selectedGameWeekName
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.gameWeekName(index: row)
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectGameWeek(index: row)
        dropdownMenu.reloadComponent(0)
        dropdownMenu.closeAllComponents(animated: true)
    }
    
}

extension PremierTableVC: PremierTableView {
    func load_data_success() {
        gameWeekDropdown.reloadComponent(0)
        tableView.reloadData()
    }
    
    
    func ShowSpinner() {
        StartLoading()
    }
    func HideSpinner() {
        StopLoading()
    }
    func failedToLoadData(error: String) {
        InfoAlert(title: "", message: error)
    }
    
    
}
