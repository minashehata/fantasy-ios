//
//  PremierTableInteractor.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 31/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Alamofire
import RxSwift

struct PremierTableInteractor {
    
    func loadGameWeeks(completion: @escaping (_ weeks: GenericDataArray<[GameWeek]>?, _ error: FCError?) -> ()) {
        NetworkXController.shared.request(url: URLs.all_game_weeks, method: .get) { (data, error, success) in
            if success  {
                let weeks = try? JSONDecoder().decode(GenericDataArray<[GameWeek]>.self, from: data!)
                completion(weeks, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
}
