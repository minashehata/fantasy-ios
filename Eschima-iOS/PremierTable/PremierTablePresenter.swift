//
//  PremierTablePresenter.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 31/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxCocoa
import RxSwift

protocol PremierTableView: AnyObject, SpinnerView {
    func load_data_success()
}

final class PremierTablePresenter: NSObject {
    
    // Variables....
    private weak var view: PremierTableView?
    private let interactor = PremierTableInteractor()
    let dataSource = BehaviorRelay<[Fixture]>(value: [])
    private let bag = DisposeBag()

    private var selectedGameWeek: GameWeek?
    var selectedGameWeekName: String?
    private var fixtures: [Fixture]
    
    private var gameWeeks: [GameWeek]
    
    init(view: PremierTableView) {
        self.view = view
        self.gameWeeks = []
        self.fixtures = []
        super.init()
    }

    
    
    func loadAllGameWeeks() {
        view?.ShowSpinner()
        interactor.loadGameWeeks { [weak self] (data, error) in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let data = data {
                self.gameWeeks = data.data ?? []
                self.selectedGameWeek = self.gameWeeks.first
                self.selectedGameWeekName = self.selectedGameWeek?.name
                if let gameWeek = data.data?.first {
                    self.dataSource.accept(gameWeek.fixtures)
                    self.view?.load_data_success()
                }
            }else {
                self.dataSource.accept([])
            }
        }
    }
    var gameWeeksCount: Int {
        return gameWeeks.count
    }
    func gameWeekName(index: Int) -> String {
        return gameWeeks[index].name ?? ""
    }
    func didSelectGameWeek(index: Int) {
        selectedGameWeek = gameWeeks[index]
        selectedGameWeekName = selectedGameWeek?.name
        dataSource.accept(selectedGameWeek?.fixtures ?? [])
        view?.load_data_success()
    }
    
}
