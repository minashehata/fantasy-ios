//
//  TableArrangement.swift
//  Eschima-iOS
//
//  Created by Mina on 05/06/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import SpreadsheetView

class TableArrangementVC: SuperViewController, SpreadsheetViewDelegate {
    
    @IBOutlet weak var headerView: UIView!
    var presenter: TableArrangementPresenter!
    
    lazy var spreadView: SpreadsheetView = {
       let sv = SpreadsheetView()
        sv.backgroundColor = .clear
        return sv
    }()
    
    override func setupUI() {
        presenter = TableArrangementPresenter(view: self)
        view.addSubview(spreadView)
        spreadView.anchor(top: headerView.bottomAnchor, leading: view.safeAreaLayoutGuide.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, trailing: view.safeAreaLayoutGuide.trailingAnchor, padding: .init(top: 4, left: 16, bottom: 16, right: 16))
        spreadView.register(UINib(nibName: "ArrangementCell", bundle: nil), forCellWithReuseIdentifier: "ArrangementCell")
        spreadView.dataSource = self
        spreadView.bounces = false

    }
    
    override func fetchData() {
        presenter.load_arrangement_table()
    }
    
}

extension TableArrangementVC: TableArrangementView {
    func load_data_success() {
        spreadView.reloadData()
    }
    
    func error_load_data(error: String) {
        InfoAlert(title: "", message: error)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    
}

extension TableArrangementVC: SpreadsheetViewDataSource {
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 50
    }
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 170
        }
        return 70
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 9
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return presenter.data_count + 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return presenter.data_count > 0 ? 1 : 0
    }
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return presenter.data_count > 0 ? 1 : 0
    }
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: "ArrangementCell", for: indexPath) as! ArrangementCell
        presenter.configure(cell: cell, row: indexPath.row, colomn: indexPath.column)
        return cell
    }
    
    
    
}


