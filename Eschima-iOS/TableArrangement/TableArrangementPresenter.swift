//
//  TableArrangementPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 05/06/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
protocol TableArrangementView: AnyObject, SpinnerView {
    func load_data_success()
    func error_load_data(error: String)
}

protocol ArrangementCellView: AnyObject {
    
    func display_name(number: String)
    func display_color(color: UIColor)
    func display_text_color(color: UIColor)
    
}

final class TableArrangementPresenter: NSObject {
    
    private weak var view: TableArrangementView?
    private let interactor = TableArrangementInteractor()
    private var data: [PremierItem]
    
    init(view: TableArrangementView) {
        self.view = view
        self.data = []
        super.init()
    }
    
    func load_arrangement_table() {
        view?.ShowSpinner()
        interactor.load_table_arrangement { [weak self] data, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let data = data {
                self.data = data
                self.view?.load_data_success()
            } else {
                self.view?.error_load_data(error: error ?? "")
            }
            
        }
    }
    
    var data_count: Int {
        return data.count
    }
    
    func configure(cell: ArrangementCellView, row: Int, colomn: Int) {
        
        if row == 0, colomn == 0 {
            cell.display_name(number: "Team")
            cell.display_color(color: UIColor.FRedColor)
            cell.display_text_color(color: UIColor.FDarkGreen)
        } else if row == 0, colomn == 1 {
            cell.display_name(number: "MP")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 2 {
            cell.display_name(number: "W")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 3 {
            cell.display_name(number: "D")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 4 {
            cell.display_name(number: "L")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 5 {
            cell.display_name(number: "Pts")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 6 {
            cell.display_name(number: "GF")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 7 {
            cell.display_name(number: "GA")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if row == 0, colomn == 8 {
            cell.display_name(number: "GD")
            cell.display_color(color: UIColor.FDarkGreen)
            cell.display_text_color(color: UIColor.white)
        } else if colomn == 0 {
            cell.display_name(number: "\(row)   \(data[row - 1].team?.name ?? "")" )
            cell.display_text_color(color: .black)
            cell.display_color(color: .FGrayColor)
        } else {
            if colomn == 1 {
                cell.display_name(number: "\(data[row - 1].played ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 2 {
                cell.display_name(number: "\(data[row - 1].won ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 3 {
                cell.display_name(number: "\(data[row - 1].drawn ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 4 {
                cell.display_name(number: "\(data[row - 1].lost ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 5 {
                cell.display_name(number: "\(data[row - 1].points ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 6 {
                cell.display_name(number: "\(data[row - 1].score ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 7 {
                cell.display_name(number: "\(data[row - 1].receive ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            } else if colomn == 7 {
                cell.display_name(number: "\(data[row - 1].goalDifference ?? 0)")
                cell.display_text_color(color: .darkText)
                cell.display_color(color: .clear)
            }
        }
        
    }
    
    
}
