//
//  ArrangementCell.swift
//  Eschima-iOS
//
//  Created by Mina on 05/06/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import SpreadsheetView

class ArrangementCell: Cell, ArrangementCellView {
 

    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func display_name(number: String) {
        nameLabel.text = number
    }
    
    func display_color(color: UIColor) {
        nameLabel.backgroundColor = color
    }
    
    func display_text_color(color: UIColor) {
        nameLabel.textColor = color
    }
    
    
}
