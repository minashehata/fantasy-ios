//
//  TableArrangementInteractor.swift
//  Eschima-iOS
//
//  Created by Mina on 05/06/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

struct TableArrangementInteractor {
    
    func load_table_arrangement(completion: @escaping (_ items: [PremierItem]?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.table_arrangement, method: .get) { data, success in
            if !success {
                completion(nil, data?["msg"].string)
            }
            if let data = data {
                let items = try? JSONDecoder().decode(GenericDataArray<[PremierItem]>.self, from: data.rawData())
                completion(items?.data, nil)
            }
        }
    }
}
