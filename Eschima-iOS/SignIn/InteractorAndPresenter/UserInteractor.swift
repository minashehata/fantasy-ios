
import Foundation
import Alamofire

typealias userCompletion = (_ User: User?, _ error: String?) -> ()

class UserInteractor: NSObject {

    
    private var user: User?
    
    func signIn(emailOrPhone: String, password: String, completion: @escaping userCompletion) {
        //HTTPHeader(name: "fcmToken", value: (UserDefaults.standard.value(forKey: "fcm_token") as? String) ?? "")]

        let headers: HTTPHeaders = [HTTPHeader(name: "emailOrPhone", value: emailOrPhone), HTTPHeader(name: "password", value: password)]
        NetworkLoader.shared.request(url: URLs.login, method: .post, paramheaders: headers) { [weak self] (item, success) in
            guard let self = self else { return }
            if !success {
                completion(nil, "Error in email or password")
                return
            }
            if let item = item {
                let user = User(with: item["data"])
                self.user = user
                self.saveUser()
                print("ssss", UserInteractor.getUser()?.token ?? "")
                print("email", UserInteractor.getUser()?.email ?? "")
                completion(user, nil)
            }
        }
    }
    
    
    func register(country: String, emailOrPhone: String, mobile: String, password: String, teamId: String, teamName: String, completion: @escaping (_ message: String?, _ error: String?) -> ()) {

        let headers: HTTPHeaders = [HTTPHeader(name: "email", value: emailOrPhone), HTTPHeader(name: "phone", value: mobile), HTTPHeader(name: "password", value: password), HTTPHeader(name: "country", value: country), HTTPHeader(name: "teamId", value: teamId) , HTTPHeader(name: "teamName", value: teamName)]
        NetworkLoader.shared.request(url: URLs.register, method: .post, paramheaders: headers) { [weak self] (item, success) in
            if !success {
                completion(nil, item?["msg"].string)
                return
            }
            if let item = item {
                completion(item["msg"].string, nil)
            }
        }
    }
    
//    func get_current_user(completion: @escaping (_ User: TeacherDTO?, _ error: String?) -> ()) {
//        NetworkLoader.shared.request(url: URLs.current_user, method: .get) { [weak self] (data, success) in
//            guard let self = self else { return }
//            if !success {
//                completion(nil, "Error in email or password")
//                return
//            }
//            if let data = data {
//                let user = TeacherDTO(json: data["data"])
//                completion(user, nil)
//            }
//        }
//    }
    
    @discardableResult
    func saveUser() -> Bool {
        let userDefaults = UserDefaults.standard
        let archivedUser = try! NSKeyedArchiver.archivedData(withRootObject: user!, requiringSecureCoding: false)
        userDefaults.set(archivedUser, forKey: "User")
        return userDefaults.synchronize()
    }

    class func getUser() -> User? {
        let userDefaults = UserDefaults.standard
        if let userData = userDefaults.value(forKey: "User") as? Data {
            if let user = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(userData) as? User {
                return user
            }
        }
        return nil
    }

    @discardableResult
    class func removeUser() -> Bool {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: "User")
        UserDefaults.standard.removeObject(forKey: "fcm_token")
        UserDefaults.standard.synchronize()
        userDefaults.synchronize()
        Router.restartApp()
        return true
    }
    
}
