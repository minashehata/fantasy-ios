//
//  UserPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 13/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol UserView: AnyObject, SpinnerView {
    func load_user_success(user: User?)
    func error_msg(error: String)
}
final class UserPresenter: NSObject {
    
    private weak var view: UserView?
    private let interactor = UserInteractor()
        
     var selectedCountry: String?
    init(view: UserView) {
        self.view = view
        super.init()
    }
    
    
    
    func validateLogin(phone: String, password: String, completion: (_ success: Bool, _ title: String?, _ error: String?) -> ()) {
        guard !phone.isEmpty else {
            completion(false, "".Localize, "Enter email address".Localize)
            return
        }
        
        guard !password.isEmpty else {
            completion(false, "".Localize, "Enter password".Localize)
            return
        }
        completion(true, nil, nil)
    }
    
    
    func validateRigester(email: String, phone: String, password: String, teamId: String, teamName: String, completion: (_ success: Bool, _ title: String?, _ error: String?) -> ()) {
        guard !phone.isEmpty else {
            completion(false, "".Localize, "Enter phone".Localize)
            return
        }
        guard !email.isEmpty else {
            completion(false, "".Localize, "Enter email address".Localize)
            return
        }
        guard selectedCountry != nil else {
            completion(false, "".Localize, "Select your country".Localize)
            return
        }
        
        guard !password.isEmpty else {
            completion(false, "".Localize, "Enter password".Localize)
            return
        }
        
        guard !teamId.isEmpty else {
            completion(false, "".Localize, "Enter Team ID".Localize)
            return
        }
        
        guard !teamName.isEmpty else {
            completion(false, "".Localize, "Enter Team Name".Localize)
            return
        }
        completion(true, nil, nil)
    }
    
    
    
    
    func login(email: String, password: String) {
        view?.ShowSpinner()
        interactor.signIn(emailOrPhone: email, password: password) { [weak self] user, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let user = user {
                self.view?.load_user_success(user: user)
            } else {
                self.view?.error_msg(error: error ?? "")
            }
        }
    }
    
    func register(email: String, phone: String, password: String, teamId: String, teamName: String) {
        view?.ShowSpinner()
        interactor.register(country: selectedCountry ?? "", emailOrPhone: email, mobile: phone, password: password, teamId: teamId, teamName: teamName) { [weak self] message, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let message = message {
                self.view?.load_user_success(user: nil)
            } else {
                self.view?.error_msg(error: error ?? "")
            }
        }
    }
    
}
