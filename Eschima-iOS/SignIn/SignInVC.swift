//
//  SignInVC.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 25/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SignInVC: SuperViewController {

    var presenter: UserPresenter!
    
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var passwordTF: UITextFieldX! {
        didSet {
            passwordTF.textAlignment = Language.currentLanguage().contains("ar") ? .right : .left
        }
    }
    let bag = DisposeBag()
    @IBOutlet weak var signInButton: UIButtonX!
    
    override func setupUI() {
        presenter = UserPresenter(view: self)
        bindLoginUserButton()
    }
    
    override func fetchData() {
        
    }
    
    
    
    private func bindLoginUserButton() {
        signInButton.rx.tap
            .do(onNext:  { [weak self] in
                guard let self = self else { return }
                self.presenter.validateLogin(phone: self.emailTF.text ?? "", password: self.passwordTF.text ?? "") { [weak self] success, title, message in
                    guard let self = self else { return }
                    if success {
                        self.presenter.login(email: self.emailTF.text ?? "", password: self.passwordTF.text ?? "")
                    } else {
                        self.InfoAlert(title: title ?? "", message: message ?? "")
                    }
                }
            }).subscribe()
            .disposed(by: bag)
    }
    
    
    
}


extension SignInVC: UserView {
    func load_user_success(user: User?) {
        Transition.goToStoryboard(with: "Home")
    }
    
    func error_msg(error: String) {
        InfoAlert(title: "", message: error)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    
}
