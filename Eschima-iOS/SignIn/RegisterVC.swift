//
//  RegisterVC.swift
//  Eschima-iOS
//
//  Created by Mina on 14/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxSwift
import UIKit
import Planet

class RegisterVC: SuperViewController {

    var presenter: UserPresenter!
    private let bag = DisposeBag()
    
    @IBOutlet weak var countryTF: UITextFieldX!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var teamIdTF: UITextFieldX!
    @IBOutlet weak var teamNameTF: UITextFieldX!

    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var mobileTF: UITextFieldX!
    @IBOutlet weak var passwordTF: UITextFieldX! {
        didSet {
            passwordTF.textAlignment = Language.currentLanguage().contains("ar") ? .right : .left
        }
    }
    
    @IBOutlet weak var registerButton: UIButtonX!
    
    override func setupUI() {
        presenter = UserPresenter(view: self)
        bindTapRegisterButtonPressed()
        bindCountryButtonPressed()
    }
    
    override func fetchData() {
        
    }

    private func bindTapRegisterButtonPressed() {
        registerButton.rx.tap
            .do(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.presenter.validateRigester(email: self.emailTF.text ?? "", phone: self.mobileTF.text ?? "", password: self.passwordTF.text ?? "", teamId: self.teamIdTF.text ?? "", teamName: self.teamNameTF.text ?? "") { [weak self] success, title, message in
                    guard let self = self else { return }
                    self.presenter.register(email: self.emailTF.text ?? "", phone: self.mobileTF.text ?? "", password: self.passwordTF.text ?? "", teamId: self.teamIdTF.text ?? "", teamName: self.teamNameTF.text ?? "")
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
    
    private func bindCountryButtonPressed() {
        countryButton.rx.tap
            .do(onNext: { [weak self] _ in
                guard let self = self else { return }
                self.openCountryPickerViewController()
            })
            .subscribe()
            .disposed(by: bag)
    }
    private func openCountryPickerViewController() {
        let cv = CountryPickerViewController()
        cv.delegate = self
        present(cv, animated: true, completion: nil)
    }
}

extension RegisterVC: CountryPickerViewControllerDelegate {
    func countryPickerViewControllerDidCancel(_ countryPickerViewController: CountryPickerViewController) {
        countryPickerViewController.dismiss(animated: true, completion: nil)
    }
    
    func countryPickerViewController(_ countryPickerViewController: CountryPickerViewController, didSelectCountry country: Country) {
        countryTF.text = country.name
        presenter.selectedCountry = country.name
        countryPickerViewController.dismiss(animated: true, completion: nil)
    }
    
 

}

extension RegisterVC: UserView {
    
    func load_user_success(user: User?) {
        SuccessAlert(title: "", message: "تم انشاء مستخدم جديد قم بتسجيل الدخول الان")
        navigationController?.popViewController(animated: true)
    }
    
    func error_msg(error: String) {
        InfoAlert(title: "", message: error)
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
}
