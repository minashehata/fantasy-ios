//
//  GameWeekVC.swift
//  Eschima-iOS
//
//  Created by Mina on 17/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit
import RxSwift
import MKDropdownMenu

class GameWeekVC: SuperViewController {
    private let bag = DisposeBag()
    var presenter: GameWeekPresenter!
    @IBOutlet weak var gameweekSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var newsTableView: UITableViewX!
    //
    @IBOutlet weak var bestTableView: UITableViewX!
    @IBOutlet weak var bestHeaderView: UIView!
    @IBOutlet weak var bestSegmentControl: UISegmentedControl!
    
    //
    @IBOutlet weak var captinTableView: UITableViewX!
    @IBOutlet weak var captinHeaderTableView: UIView!
    @IBOutlet weak var captinImageView1: UIImageViewX!
    @IBOutlet weak var captinImageView2: UIImageViewX!
    @IBOutlet weak var captinImageView3: UIImageViewX!
    @IBOutlet weak var captinImageView4: UIImageViewX!
    
    //
    @IBOutlet weak var teamsTableView: UITableViewX!
    @IBOutlet weak var teamsHeaderTableView: UIView!
    @IBOutlet weak var teamsDropDown: MKDropdownMenu! {
        didSet {
            teamsDropDown.delegate = self
            teamsDropDown.dataSource = self
            setupDropdown(dropdown: teamsDropDown)
        }
    }
    
    override func setupUI() {
        presenter = GameWeekPresenter(view: self)
        bestTableView.isHidden = true
        captinTableView.isHidden = true
        teamsTableView.isHidden = true
        setupTableView()
        bindSegmentControllChanged()
        bindBestHistSegmentChanged()
    }
    
    private func setupTableView() {
        newsTableView.delegate = self
        newsTableView.dataSource = self
        newsTableView.register(UINib(nibName: "\(NewsCell.self)", bundle: nil), forCellReuseIdentifier: "\(NewsCell.self)")
        
        bestTableView.delegate = self
        bestTableView.dataSource = self
        bestTableView.tableHeaderView = bestHeaderView
        bestTableView.register(UINib(nibName: "\(TeamTypeCell.self)", bundle: nil), forCellReuseIdentifier: "\(TeamTypeCell.self)")
        
        //
        captinTableView.delegate = self
        captinTableView.dataSource = self
        captinTableView.tableHeaderView = captinHeaderTableView
        captinTableView.register(UINib(nibName: "\(CaptinCell.self)", bundle: nil), forCellReuseIdentifier: "\(CaptinCell.self)")
            //
        teamsTableView.delegate = self
        teamsTableView.dataSource = self
        teamsTableView.tableHeaderView = teamsHeaderTableView
        teamsTableView.register(UINib(nibName: "\(TeamCreationCell.self)", bundle: nil), forCellReuseIdentifier: "\(TeamCreationCell.self)")
    }
    private func bindSegmentControllChanged() {
        gameweekSegmentControl.rx.controlEvent(.valueChanged)
            .do(onNext: { [weak self] _ in
                guard let self = self else { return }
                let type = self.presenter.get_set_type(index: self.gameweekSegmentControl.selectedSegmentIndex)
                switch type {
                case .news:
                    self.newsTableView.reloadData()
                    self.bestTableView.isHidden = true
                    self.captinTableView.isHidden = true
                    self.teamsTableView.isHidden = true
                    self.newsTableView.isHidden = false
                case .best:
                    self.presenter.load_best_hits(index: self.bestSegmentControl.selectedSegmentIndex)
                    self.bestTableView.reloadData()
                    self.captinTableView.isHidden = true
                    self.teamsTableView.isHidden = true
                    self.newsTableView.isHidden = true
                    self.bestTableView.isHidden = false
                case .captin:
                    self.presenter.get_captin_of_week()
                    self.captinTableView.isHidden = false
                    self.teamsTableView.isHidden = true
                    self.newsTableView.isHidden = true
                    self.bestTableView.isHidden = true
                    self.captinTableView.reloadData()
                case .expected_creation:
                    self.presenter.load_teams()
                    self.teamsTableView.isHidden = false
                    self.captinTableView.isHidden = true
                    self.newsTableView.isHidden = true
                    self.bestTableView.isHidden = true
                    self.teamsTableView.reloadData()
                }
            })
            .subscribe()
            .disposed(by: bag)
    }
    override func fetchData() {
        presenter.load_news()
    }
    
    func bindBestHistSegmentChanged() {
        bestSegmentControl.rx.controlEvent(.valueChanged)
            .do(onNext: { [weak self] in
                guard let self = self else { return }
                self.presenter.load_best_hits(index: self.bestSegmentControl.selectedSegmentIndex)
            })
            .subscribe()
            .disposed(by: bag)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? NewInternalVC {
            vc.news = sender as? News
        }
        if let vc = segue.destination as? YoutubeVideoController {
            vc.url = sender as? String
        }
    }
}

extension GameWeekVC: GameWeekView {
    func reload_drop_down() {
        teamsDropDown.reloadComponent(0)
        teamsDropDown.closeAllComponents(animated: true)
    }
    
    
    func reload_news() {
        newsTableView.reloadData()
        bestTableView.reloadData()
        captinTableView.reloadData()
        teamsTableView.reloadData()
    }
    
    func error_message(msg: String) {
        InfoAlert(title: "", message: msg)
    }
    
    func update_UI(captin: CaptinWeek) {
        NetworkLoader.shared.downloadImageToImageView(imageView: &captinImageView1, url: captin.player1?.photo ?? "")
        NetworkLoader.shared.downloadImageToImageView(imageView: &captinImageView2, url: captin.player2?.photo ?? "")
        NetworkLoader.shared.downloadImageToImageView(imageView: &captinImageView3, url: captin.player3?.photo ?? "")
        NetworkLoader.shared.downloadImageToImageView(imageView: &captinImageView4, url: captin.player4?.photo ?? "")
        
    }
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
}
extension GameWeekVC: MKDropdownMenuDelegate, MKDropdownMenuDataSource {
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 1
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return presenter.teams_count
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForComponent component: Int) -> String? {
        return presenter.selected_creation
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        return presenter.team_name(index: row)
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        presenter.didSelectExpectedCreation(index: row)
        presenter.get_expected_creation()
    }
    
}
extension GameWeekVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch presenter.get_set_type(index: gameweekSegmentControl.selectedSegmentIndex) {
        case .news:
            return presenter.news_count
        case .best:
            return presenter.team_types_count
        case .captin:
            return presenter.captin_count
        case .expected_creation:
            return presenter.creations_count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch presenter.get_set_type(index: gameweekSegmentControl.selectedSegmentIndex) {
        case .news:
            let cell = newsTableView.dequeueReusableCell(withIdentifier: "\(NewsCell.self)", for: indexPath) as! NewsCell
            cell.setupUI(news: presenter.get_news(index: indexPath.row))
            cell.didPressReadMore = { [weak self] in
                guard let self = self else { return }
                self.performSegue(withIdentifier: Constants.showNewsInternalSegue, sender: self.presenter.get_news(index: indexPath.row))
            }
            return cell
        case .best:
            let cell = bestTableView.dequeueReusableCell(withIdentifier: "\(TeamTypeCell.self)", for: indexPath) as! TeamTypeCell
            presenter.configure_best(cell: cell, index: indexPath.row)
            return cell
        case .captin:
            let cell = captinTableView.dequeueReusableCell(withIdentifier: "\(CaptinCell.self)", for: indexPath) as! CaptinCell
            cell.setupUI(item: presenter.get_item_video(index: indexPath.row))
            cell.didPressOnVideoURL = { [weak self] in
                guard let self = self else { return }
                self.performSegue(withIdentifier: "OpenVideoYoutubeSegue", sender: self.presenter.get_item_video(index: indexPath.row).video)
            }
            return cell
        case .expected_creation:
            let cell = teamsTableView.dequeueReusableCell(withIdentifier: "\(TeamCreationCell.self)", for: indexPath) as! TeamCreationCell
            cell.setupUI(team: presenter.get_creation(index: indexPath.row))
            return cell
        }
    }
}
