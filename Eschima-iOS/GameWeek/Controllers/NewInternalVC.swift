//
//  NewInternalVC.swift
//  Eschima-iOS
//
//  Created by Mina on 21/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class NewInternalVC: SuperViewController {

    var news: News?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageViewX!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    override func setupUI() {
        titleLabel.text = news?.title
        NetworkLoader.shared.downloadImageToImageView(imageView: &newsImageView, url: news?.imagePath ?? "")
        bodyLabel.text = news?.newsText
    }
    
    override func fetchData() {
        
    }

}
