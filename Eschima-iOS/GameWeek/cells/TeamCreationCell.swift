//
//  TeamCreationCell.swift
//  Eschima-iOS
//
//  Created by Mina on 22/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class TeamCreationCell: UITableViewCell {

    @IBOutlet weak var teamImageView: UIImageViewX!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    func setupUI(team: ExpectedCreation) {
        NetworkLoader.shared.downloadImageToImageView(imageView: &teamImageView, url: team.imagePath ?? "")
    }
    
}
