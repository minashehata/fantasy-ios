//
//  PlayerCell.swift
//  Eschima-iOS
//
//  Created by Mina on 21/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class PlayerCell: UICollectionViewCell {
    @IBOutlet weak var playerImageView: UIImageViewX!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var teamLabeel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setupUI(player: Player) {
        NetworkLoader.shared.downloadImageToImageView(imageView: &playerImageView, url: player.photo ?? "")
        playerNameLabel.text = player.web_name
        teamLabeel.text = player.playerTeam
    }

}
