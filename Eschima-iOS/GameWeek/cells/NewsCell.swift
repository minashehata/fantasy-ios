//
//  NewsCell.swift
//  Eschima-iOS
//
//  Created by Mina on 17/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsImageView: UIImageViewX!
    @IBOutlet weak var newsTitle: UILabel!
    
    @IBOutlet weak var newsDescLabel: UILabel!
    var didPressReadMore: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    @IBAction func readMoreButtonPressed(_ sender: UIButtonX) {
        if let didPressReadMore = didPressReadMore {
            didPressReadMore()
        }
    }
    func setupUI(news: News) {
        newsTitle.text = news.title
        newsDescLabel.text = news.newsText
        NetworkLoader.shared.downloadImageToImageView(imageView: &newsImageView, url: news.imagePath ?? "")
    }
    
}
