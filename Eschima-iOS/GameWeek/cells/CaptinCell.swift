//
//  CaptinCell.swift
//  Eschima-iOS
//
//  Created by Mina on 21/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class CaptinCell: UITableViewCell {
    @IBOutlet weak var DescLabel: UILabelX!
    @IBOutlet weak var videoImageView: CustomImageView!
    var didPressOnVideoURL: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func setupUI(item: CaptinItem) {
        DescLabel.text = item.desc
//        videoImageView.loadProfileImageWithUrl(url: URL(string: item.video)!)
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if let didPressOnVideoURL = didPressOnVideoURL {
            didPressOnVideoURL()
        }
    }
    
}

struct CaptinItem {
    var desc: String
    var video: String
}
