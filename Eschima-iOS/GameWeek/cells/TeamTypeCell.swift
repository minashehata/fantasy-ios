//
//  TeamTypeCell.swift
//  Eschima-iOS
//
//  Created by Mina on 21/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class TeamTypeCell: UITableViewCell, TeamTypeCellView {
   
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var typeTitleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var data: [Player] = []
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "\(PlayerCell.self)", bundle: nil), forCellWithReuseIdentifier: "\(PlayerCell.self)")
    }
    func display_goal_keepers(goalKeeper: [Player]) {
        data = goalKeeper
        collectionView.reloadData()
    }
    
    func display_defenders(defenders: [Player]) {
        data = defenders
        collectionView.reloadData()
    }
    
    func display_midfielders(midfielders: [Player]) {
        data = midfielders
        collectionView.reloadData()
    }
    
    func display_forwards(forwards: [Player]) {
        data = forwards
        collectionView.reloadData()
    }
    
    func display_header(name: String) {
        if name == "" {
            headerView.isHidden = true
        } else {
            headerView.isHidden = false
            typeTitleLabel.text = name
        }
    }
}

extension TeamTypeCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(PlayerCell.self)", for: indexPath) as! PlayerCell
        cell.setupUI(player: data[indexPath.item])
        return cell
    }
    
}
