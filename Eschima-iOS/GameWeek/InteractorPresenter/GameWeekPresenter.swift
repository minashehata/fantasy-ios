//
//  GameWeekPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 17/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

enum GameweekType {
    case news
    case best
    case captin
    case expected_creation
}

protocol TeamTypeCellView: AnyObject {
    func display_goal_keepers(goalKeeper: [Player])
    func display_defenders(defenders: [Player])
    func display_midfielders(midfielders: [Player])
    func display_forwards(forwards: [Player])
    func display_header(name: String)
}

protocol GameWeekView: AnyObject, SpinnerView {
    func reload_news()
    func reload_drop_down()
    func update_UI(captin: CaptinWeek)
    func error_message(msg: String)
}

final class GameWeekPresenter: NSObject {
    
    private weak var view: GameWeekView?
    private let interactor = GameWeekInteractor()
    private var newsDataSource = [News]()
    private var type: GameweekType = GameweekType.news
    
    init(view: GameWeekView) {
        self.view = view
        super.init()
    }
    func get_set_type(index: Int) -> GameweekType {
        switch index {
        case 0:
            type = .news
        case 1:
            type = .best
        case 2:
            type = .captin
        case 3:
            type = .expected_creation
        default:
            type = .news
        }
        return type
    }
    func load_news() {
        view?.ShowSpinner()
        interactor.load_news { [weak self] news, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let news = news {
                self.newsDataSource = news
                self.view?.reload_news()
            }
            else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    func get_news(index: Int) -> News{
        return newsDataSource[index]
    }
    var news_count: Int {
        return newsDataSource.count
    }
    //
    var team: BestTeam?
    
    func load_best_hits(index: Int) {
        view?.ShowSpinner()
        interactor.getBestTeamByIndex(index: index) { [weak self] team, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let team = team {
                self.team = team
                self.view?.reload_news()
            } else {
                self.view?.error_message(msg: error ?? "")
            }
            
        }
    }
    var team_types_count: Int {
        return 4
    }
    
    func configure_best(cell: TeamTypeCellView, index: Int) {
        if index == 0 {
            cell.display_header(name: "حراسة المرمى")
            cell.display_goal_keepers(goalKeeper: team?.goalKeeper ?? [])
        } else if index == 1 {
            cell.display_header(name: "خط الظهر")
            cell.display_defenders(defenders: team?.defenders ?? [])
        } else if index == 2 {
            cell.display_header(name: "خط الوسط")
            cell.display_midfielders(midfielders: team?.midfielders ?? [])
        } else {
            cell.display_header(name: "خط الهجوم")
            cell.display_forwards(forwards: team?.forwards ?? [])
        }
        
    }
    
    //
    var captin_count: Int {
        return items.count
    }
    var captin: CaptinWeek?
    private var items: [CaptinItem] = []
    func get_captin_of_week() {
        view?.ShowSpinner()
        interactor.GetCaptainsByGameWeek { [weak self] captin, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let captin = captin {
                self.captin = captin
                self.items = [CaptinItem(desc: captin.description1 ?? "", video: captin.videoUrl1 ?? ""), CaptinItem(desc: captin.description2 ?? "", video: captin.videoUrl2 ?? ""),
                              CaptinItem(desc: captin.description3 ?? "", video: captin.videoUrl3 ?? ""),
                              CaptinItem(desc: captin.description4 ?? "", video: captin.videoUrl4 ?? "")]
                self.view?.update_UI(captin: captin)
                self.view?.reload_news()
            } else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    
    func get_item_video(index: Int) -> CaptinItem {
        return items[index]
    }
    
    // expected creation
    private var teams = [Team]()
    var selected_creation: String = "اختر فريق"
    func load_teams() {
        view?.ShowSpinner()
        interactor.get_teams { [weak self] teams, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let teams = teams {
                self.teams = teams
                self.view?.reload_drop_down()
            } else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    
    var teams_count: Int {
        return teams.count
    }
    func team_name(index: Int) -> String {
        return teams[index].name ?? ""
    }
    func didSelectExpectedCreation(index: Int) {
        selected_creation = teams[index].name ?? ""
        self.view?.reload_drop_down()
    }
    private var creations: [ExpectedCreation] = []
    func get_expected_creation() {
        guard selected_creation != "اختر فريق" else {
            view?.error_message(msg: "اختر فريق")
            return
        }
        view?.ShowSpinner()
        interactor.get_expected_creation(team_name: selected_creation) { [weak self] creations, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let creations = creations {
                self.creations = creations
                self.view?.reload_news()
            } else {
                self.view?.error_message(msg: error ?? "")
            }
        }
    }
    var creations_count: Int {
        return creations.count
    }
    
    func get_creation(index: Int) -> ExpectedCreation {
        return creations[index]
    }
}
