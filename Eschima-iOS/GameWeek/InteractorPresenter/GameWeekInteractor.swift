//
//  GameWeekInteractor.swift
//  Eschima-iOS
//
//  Created by Mina on 17/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation

struct GameWeekInteractor {
    
    func load_news(completion: @escaping (_ news: [News]?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.load_news + "?gameweek=1", method: .get) { data, success in
            if (!success) {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let news: [News] = data["data"].map { return News(json: $0.1) }
                completion(news, nil)
            }
        }
    }
    
    func getBestTeamByIndex(index: Int, completion: @escaping (_ bestTeam: BestTeam?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: (index == 0 ? URLs.free_hits : URLs.wild_card) + "?gameweek=1", method: .get) { data, success in
            if (!success) {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let team = BestTeam(json: data["data"])
                completion(team, nil)
            }
        }
    }
    
    func GetCaptainsByGameWeek(completion: @escaping (_ captin: CaptinWeek?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.captin_week + "?gameweek=1", method: .get) { data, success in
            if (!success) {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let captin = CaptinWeek(json: data["data"])
                completion(captin, nil)
            }
        }
    }
    
    func get_teams(completion: @escaping (_ teams: [Team]?, _ error: String?) -> ()) {
        NetworkXController.shared.request(url: URLs.teams, method: .get) { data, error, success in
            if !success {
                completion(nil, error?.localizedDescription)
            }
            if let data = data {
                let teams = try? JSONDecoder().decode(GenericDataArray<[Team]>.self, from: data)
                completion(teams?.data, nil)
            }
        }
    }
    
    func get_expected_creation(team_name: String, completion: @escaping (_ creation: [ExpectedCreation]?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.expected_creation + "?gameWeek=1&teamName=\(team_name)", method: .get) { data, success in
            if !success {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let creation = data["data"].map { return ExpectedCreation(json: $0.1) }
                completion(creation, nil)
            }
        }
    }
    
}
