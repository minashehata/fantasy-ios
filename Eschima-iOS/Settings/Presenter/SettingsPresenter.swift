//
//  SettingsPresenter.swift
//  Eschima-iOS
//
//  Created by Mina on 22/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import Foundation
import SwiftyJSON
protocol SettingsView: AnyObject, SpinnerView {
    func updateUI(settings: Setting)
}

final class SettingsPresenter: NSObject {
    
    private weak var view: SettingsView?
    private let interactor = SettingInteractor()
    
    init(view: SettingsView) {
        self.view = view
        super.init()
    }
    
    func load_settings() {
        view?.ShowSpinner()
        interactor.load_settings { [weak self] settings, error in
            guard let self = self else { return }
            self.view?.HideSpinner()
            if let settings = settings {
                self.view?.updateUI(settings: settings)
            } else {
                
            }
        }
    }
    
}


struct Setting {
    var aboutUs: String?
    var privacy: String?
    var contactUs: String?
    var leagueCode: String?
    
    init(json: JSON) {
        self.aboutUs = json["aboutUs"].string
        self.privacy = json["privacy"].string
        self.contactUs = json["contactUs"].string
        self.leagueCode = json["leagueCode"].string
    }
}

struct SettingInteractor {
    func load_settings(completion: @escaping (_ settings: Setting?, _ error: String?) -> ()) {
        NetworkLoader.shared.request(url: URLs.settings, method: .get) { data, success in
            if !success {
                completion(nil, data?["msg"].string)
                return
            }
            if let data = data {
                let settings = Setting(json: data["data"])
                completion(settings, nil)
            }
        }
    }
}
