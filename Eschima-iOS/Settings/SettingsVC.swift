//
//  SettingsVC.swift
//  Eschima-iOS
//
//  Created by Mina on 22/07/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import UIKit

class SettingsVC: SuperViewController {


    @IBOutlet weak var aboutUsButton: UILabel!
    @IBOutlet weak var privacyButton: UILabel!
    @IBOutlet weak var contactUsButton: UILabel!
    var presenter: SettingsPresenter!
    override func setupUI() {
        presenter = SettingsPresenter(view: self)
    }
    
    override func fetchData() {
        presenter.load_settings()
    }

}
extension SettingsVC: SettingsView {
    
    func ShowSpinner() {
        StartLoading()
    }
    
    func HideSpinner() {
        StopLoading()
    }
    
    func updateUI(settings: Setting) {
        aboutUsButton.text = settings.aboutUs
        privacyButton.text = settings.privacy
        contactUsButton.text = settings.contactUs
    }
    
}
