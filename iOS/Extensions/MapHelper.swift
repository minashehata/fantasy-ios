//
//  MapHelper.swift
//  Medica-Egypt-iOS
//
//  Created by Mina Shehata on 2/10/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import MapKit
import CFAlertViewController
import CoreLocation

class MapsHelper: NSObject, CLLocationManagerDelegate {
    static var instance = MapsHelper()
    var userLocation: UserLocation?
    var locationManager: CLLocationManager?
    
    override init() {
        super.init()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let first = locations.first
        userLocation = UserLocation(longitude: first?.coordinate.longitude ?? 0.0, latitude: first?.coordinate.latitude ?? 0.0)
        locationManager?.stopUpdatingLocation()
    }
//    fromLatitude: Double?, fromLongitude:Double? ,
    func showActionSheet(toLatitude:Double , toLongitude:Double) {
        
      
        var googleMaps: CFAlertAction?
        var AppleMaps: CFAlertAction?
        
        googleMaps = createCFAction(title: "Google Maps".Localize, style: .Default, backgroundColor: .FDarkGreen, textColor: .white, handler: { [weak self](action) in
            self?.onGoogleMap(fromLatitude: self?.userLocation?.latitude ?? 0.0, fromLongitude: self?.userLocation?.longitude ?? 0.0, toLatitude: toLatitude, toLongitude: toLongitude)
        })
        AppleMaps = createCFAction(title: "Apple Maps".Localize, style: .Default, backgroundColor: .FRedColor, textColor: .white, handler: { [weak self](action) in
            self?.onAppleMap(fromLatitude: self?.userLocation?.latitude ?? 0.0, fromLongitude: self?.userLocation?.longitude ?? 0.0, toLatitude: toLatitude, toLongitude: toLongitude)
        })
        
        let cancelAlert = createCFAction(title: "Cancel".Localize, style: .Cancel, backgroundColor: .lightGray, handler: nil)
        CFAlert(title: "Maps".Localize, titleColor: .black, message: "", messageColor: .darkGray, textAlignment: .center, preferredStyle: .actionSheet, headerView: nil, footerView: nil, handler: nil, actions: googleMaps, AppleMaps, cancelAlert)
        
    }
    
    func onGoogleMap(fromLatitude:Double? , fromLongitude:Double? ,toLatitude:Double , toLongitude:Double)  {
        var link = ""
        if fromLatitude != nil {
            link = "comgooglemaps://?saddr=\(fromLatitude!),\(fromLongitude!)&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving"
        }else{
            link = "comgooglemaps://?saddr=&daddr=\(toLatitude),\(toLongitude)&directionsmode=driving"
        }
        //Working in Swift new versions.
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!))
        {
            UIApplication.shared.open(NSURL(string:
                link)! as URL, options: [:], completionHandler: nil)
        } else{
            // open google map on Store
            UIApplication.shared.open(NSURL(string: "itms://itunes.apple.com/us/app/google-maps/id585027354?mt=8&uo=4")! as URL, options: [:], completionHandler: nil)
        }
    }
    
    func onAppleMap(fromLatitude: Double? , fromLongitude: Double? ,toLatitude: Double , toLongitude: Double) {
        
        var link = ""
        
        if fromLatitude != nil {
            link = "http://maps.apple.com/?saddr=\(fromLatitude!),\(fromLongitude!)&daddr=\(toLatitude),\(toLongitude)"
        }else{
            link = "http://maps.apple.com/?saddr=&daddr=\(toLatitude),\(toLongitude)"
        }
        //Working in Swift new versions.
        UIApplication.shared.open(NSURL(string:
            link)! as URL, options: [:], completionHandler: nil)
    }
    
    
}
// location of user..........
struct UserLocation {
    
    let longitude: Double
    let latitude: Double
    
}
