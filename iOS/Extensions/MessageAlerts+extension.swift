//
//  NoticeAlerts+extension.swift
//  NewsApp
//
//  Created by Mina Shehata on 1/4/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import SwiftMessages

extension UIViewController {

    public func createLeftView(with image: UIImage) -> UIImageView {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 33, height: 33))
        imageView.tintColor = .white
        imageView.image = image
        return imageView
    }

    public func InfoAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let banner = MessageView.viewFromNib(layout: .cardView)
            banner.configureTheme(.info, iconStyle: .none)
            banner.configureContent(title: title, body: message)
            banner.bodyLabel?.numberOfLines = 0
            banner.configureDropShadow()
            banner.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

            banner.configureTheme(backgroundColor: .infoColor, foregroundColor: .white, iconImage: #imageLiteral(resourceName: "Info"), iconText: nil)
            banner.button?.isHidden = true
            var config = SwiftMessages.Config()
            config.presentationContext = .window(windowLevel: .alert)
            config.presentationStyle = .top
            config.dimMode = .none//.blur(style: .dark, alpha: 0.5, interactive: true)
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: banner)
        }
    }

    public func ErrorAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let banner = MessageView.viewFromNib(layout: .cardView)
            banner.configureTheme(.error, iconStyle: .none)
            banner.configureContent(title: title, body: message)
            banner.bodyLabel?.numberOfLines = 0
            banner.configureDropShadow()
            banner.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            banner.configureTheme(backgroundColor: .errorColor, foregroundColor: .white, iconImage: #imageLiteral(resourceName: "Error"), iconText: nil)
            banner.button?.isHidden = true
            var config = SwiftMessages.Config()
            config.presentationContext = .window(windowLevel: .alert)
            config.presentationStyle = .top
            config.dimMode = .none//.blur(style: .dark, alpha: 0.5, interactive: true)
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: banner)
        }
    }

    public func SuccessAlert(title: String, message: String) {
        DispatchQueue.main.async {
            let banner = MessageView.viewFromNib(layout: .cardView)
            banner.configureTheme(.success, iconStyle: .none)
            banner.configureContent(title: title, body: message)
            banner.bodyLabel?.numberOfLines = 0
            banner.configureDropShadow()
            banner.layoutMarginAdditions = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            banner.configureTheme(backgroundColor: .successColor, foregroundColor: .white, iconImage: #imageLiteral(resourceName: "Success"), iconText: nil)
            banner.button?.isHidden = true
            var config = SwiftMessages.Config()
            config.presentationContext = .window(windowLevel: .alert)
            config.presentationStyle = .top
            config.dimMode = .none//.blur(style: .dark, alpha: 0.5, interactive: true)
            config.duration = .seconds(seconds: 3)
            SwiftMessages.show(config: config, view: banner)
        }
    }

}
