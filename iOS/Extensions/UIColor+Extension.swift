//
//  UIColor+Extension.swift
//  NewsApp
//
//  Created by Mina Shehata on 1/4/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import UIKit

// MARK:- Color Extension......
extension UIColor {
    
    public static let successColor = UIColor(named: "SuccessColor")!
    public static let infoColor = UIColor(named: "InfoColor")!
    public static let errorColor = UIColor(named: "ErrorColor")!
    
    // Basic Color.....
    public static let FDarkGreen = UIColor(named: "FDarkGreen")!
    public static let FRedColor = UIColor(named: "FRedColor")!
    public static let FGrayColor = UIColor(named: "FGrayColor")!
    public static let FGreenColor = UIColor(named: "FGreenColor")!
    public static let FCyanColor = UIColor(named: "FCyanColor")!
    // social colors
    static let facebookColor = UIColor(named: "FacebookColor")!
    static let googleColor = UIColor(named: "GoogleColor")!
    static let twitterColor = UIColor(named: "TwitterColor")!

    // gray colors
    static let profileImageColor = UIColor(named: "ProfileIVColor")!
    static let tabBarBackgroundColor = UIColor(named: "TabBarBackgroundColor")!
    static let profileGrayColor = UIColor(named: "ProfileGrayColor")!
    static let vDarkGrayColorProfile = UIColor(named: "vDarkGrayColorProfile")!
    static let vCityLightNameColor = UIColor(named: "vCityLightNameColor")!
    static let placeHolderGrayColor = UIColor(named: "PlaceHolderGrayColor")!
    static let bookingProfileColor = UIColor(named: "BookingProfileColor")!
    static let settingsTextFieldLineColor = UIColor(named: "SettingsTextFieldLineColor")!
    static let serviceTypeCellColor = UIColor(named: "ServiceTypeCellColor")!
    static let serviceTypeWhiteCellColor = UIColor(named: "ServiceTypeWhiteCellColor")!
    
}

