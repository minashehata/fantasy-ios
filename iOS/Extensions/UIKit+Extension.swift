//
//  UIKit+Extension.swift
//  General Project
//
//  Created by Mina Shehata Gad on 5/3/18.
//  Copyright © 2018 Mina Shehata Gad. All rights reserved.
//

import UIKit
import SafariServices
import AVFoundation
import MKDropdownMenu
import NVActivityIndicatorView
import DGActivityIndicatorView
import DropDown

//MARK:- UIViewController Extension......
extension UIViewController: SFSafariViewControllerDelegate, NVActivityIndicatorViewable {
    
    
    //MARK:- get estimate hight and width
    open func estimateFrameForText(text: String, font: CGFloat) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: font)], context: nil)
    }
    
    // MARK:- open safari with link
    public func openLinkWithSafari(link:String)  {
        if let url = URL(string: link) {
            let safariVC = SFSafariViewController(url: url)
            UIApplication.topViewController()?.present(safariVC, animated: true, completion: nil)
            safariVC.delegate = self
        } else {
            InfoAlert(title: "", message: "Not valid url".Localize)
        }
    }

   
    public func SetStatusBarColor(Color: UIColor)  {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Color
        }
    }

    func setupDropdown(dropdown: MKDropdownMenu) {
        dropdown.dropdownCornerRadius = 5

        dropdown.backgroundColor = .FDarkGreen
        dropdown.tintColor = .white
        
        dropdown.layer.cornerRadius = 5
        dropdown.layer.masksToBounds = true
        dropdown.rowTextAlignment = Language.currentLanguage().contains("ar") ? .right : .left
        dropdown.componentSeparatorColor = UIColor.FDarkGreen
    }
    
    func StartChangeLanguage() {
        let size = CGSize(width: 50, height: 50)
        NVActivityIndicatorView.DEFAULT_COLOR = .FDarkGreen
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .FRedColor
        self.startAnimating(size, message: "Changing Language......".Localize, type: NVActivityIndicatorType.ballZigZag)
    }
    func finishChangeLanguage() {
        self.stopAnimating()
    }
    
    
    //MARK:- show dropdown menu from button
    public func showDropDownWithListStrings(anchorView: UIView, data: [String], selectionAction: @escaping SelectionClosure) {
        let dropDown = DropDown()
        dropDown.anchorView = anchorView
        dropDown.animationEntranceOptions = [.curveEaseInOut]
        dropDown.backgroundColor = .vCityLightNameColor
        if Language.currentLanguage().contains("ar") {
            dropDown.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            dropDown.customCellConfiguration = { (index, item: String, cell: DropDownCell) -> Void in
                cell.optionLabel.layer.setAffineTransform(CGAffineTransform(scaleX: -1, y: 1))
            }
        }
        dropDown.textColor = .FDarkGreen
        dropDown.textFont = UIFont.systemFont(ofSize: 15, weight: .medium)
        dropDown.dataSource = data
        dropDown.selectionAction = selectionAction
        dropDown.width = anchorView.bounds.width
        dropDown.direction = .bottom
        dropDown.cellHeight = 60
        dropDown.show()
    }
    
    public func setupCFAlert(title: String = "", message: String = "", completionHandler: @escaping () -> ()) {
        let firstAction = createCFAction(title: title, style: CFActionStyle.Default, textAlignment: .justified, backgroundColor: UIColor.FDarkGreen, textColor: .white) { (action) in
            completionHandler()
        }
        
        let cancelAction = createCFAction(title: "Cancel".Localize, style: CFActionStyle.Cancel, textAlignment: .justified, backgroundColor: UIColor.FRedColor, textColor: .black, handler: nil)
        
        CFAlert(title: title, titleColor: .black, message: message, messageColor: .darkGray, textAlignment: .center, preferredStyle: .actionSheet, headerView: nil, footerView: nil, handler: { (reason) in
        }, actions: firstAction, cancelAction)
        
    }
    
    //MARK:-  thumbnail image for video
    public func thumbnailImageViewForUrl(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnail = try imageGenerator.copyCGImage(at: CMTime(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnail)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
}

//MARK:- UINavigationController Extension......
extension UINavigationController {
    public var content: UIViewController? {
        return self.viewControllers.first
    }
}


//MARK:- UIApplication Extension......
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    //MARK:-  thumbnail image for video
    public func thumbnailImageViewForUrl(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnail = try imageGenerator.copyCGImage(at: CMTime(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnail)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    public func drawPDFfromURL(url: URL) -> UIImage? {
        guard let document = CGPDFDocument(url as CFURL) else { return nil }
        guard let page = document.page(at: 1) else { return nil }
        
        let pageRect = page.getBoxRect(.mediaBox)
        let renderer = UIGraphicsImageRenderer(size: pageRect.size)
        let img = renderer.image { ctx in
            UIColor.white.set()
            ctx.fill(pageRect)
            
            ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
            ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
            ctx.cgContext.drawPDFPage(page)
        }
        return img
    }
}

//MARK:- UIDevice extension
extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}


extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            switch to{
            case .top:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.scrollToRow(at: indexPath, at: .top, animated: animated)
                }
                break
            case .bottom:
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                break
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}

// MARK:- UIView Extension:
import Lightbox
import PDFKit

extension UIViewController {
    func openPDF(pdf: String) {
        let controller = UIViewController()
        let pdfView = PDFView()
        if let pdfDocument = PDFDocument(url: URL(string: pdf)!) {
            pdfView.displayMode = .singlePageContinuous
            pdfView.autoScales = true
            pdfView.displayDirection = .vertical
            pdfView.document = pdfDocument
        }
        controller.view.addSubview(pdfView)
        pdfView.fillSuperView()
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func openMedia(media: [[MediaType : String]]) {
        var controller: LightboxController!
        var photos = [String]()
        var vedios = [String]()
        media.forEach {
            switch $0.keys.first! {
            case .photo:
                photos.append($0.values.first!)
            case .video:
                vedios.append($0.values.first!)
            }
        }
        
        openImages(photos, vedios, startIndex: 0)
        
    }
    
}


//MARK:- UIViewController LightBox implementation....
extension UIViewController : LightboxControllerDismissalDelegate ,LightboxControllerPageDelegate{
    public func lightboxControllerWillDismiss(_ controller: LightboxController) {
        controller.dismiss(animated: true, completion: nil)
//        presentingViewController?.dismiss(animated: true, completion: nil)
        
    }
    
    public func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    func openImages(_ images:[String], _ vedios: [String], startIndex: Int, pageIndicator: Bool = true) {
        var imgs = [LightboxImage]()
        guard images.count > 0 else { return }
        imgs = images.map{LightboxImage(imageURL: URL(string:  $0)!)}
        vedios.forEach {
            imgs.append(LightboxImage(image: #imageLiteral(resourceName: "Error"), text: "", videoURL: URL(string: $0)))
        }
        LightboxConfig.DeleteButton.enabled = false
//        LightboxConfig.CloseButton.image = #imageLiteral(resourceName: "Error")
        LightboxConfig.CloseButton.text = ""
        LightboxConfig.InfoLabel.enabled = false
        LightboxConfig.PageIndicator.enabled = pageIndicator
        let lightbox = LightboxController(images: imgs , startIndex : startIndex)
        lightbox.dismissalDelegate = self
        // Set delegates.
        lightbox.pageDelegate = self
        // Use dynamic background.
//        lightbox.dynamicBackground = true
        
        let gradiantController = UINavigationController(rootViewController: lightbox)
        _ = lightbox.addLeftBarButton(image: #imageLiteral(resourceName: "Error"), selector: #selector(dismissScreen))
        present(gradiantController, animated: true, completion: nil)
    }
    
    @objc func dismissScreen() {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func openVideos(_ vedios: [String], startIndex: Int, pageIndicator: Bool = false) {
        
        let videos:[LightboxImage] = vedios.map { LightboxImage(image: #imageLiteral(resourceName: "play_button_video"), text: "", videoURL: URL(string: $0) ) }
        LightboxConfig.DeleteButton.enabled = false
        
        LightboxConfig.CloseButton.text = ""
        LightboxConfig.InfoLabel.enabled = false
        LightboxConfig.PageIndicator.enabled = pageIndicator
        let lightbox = LightboxController(images: videos , startIndex : startIndex)
        lightbox.dismissalDelegate = self
        // Set delegates.
        lightbox.pageDelegate = self
        // Use dynamic background.
        lightbox.dynamicBackground = true
        present(lightbox, animated: true, completion: nil)
    }
}
