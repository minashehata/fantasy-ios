//
//  NetworkXController.swift
//  Eschima-iOS
//
//  Created by Mina Shehata on 26/05/2021.
//  Copyright © 2021 Mina Shehata Gad. All rights reserved.
//

import RxAlamofire
import Alamofire
import RxSwift
typealias rxResponse = (_ data: Data?, _ error: FCError?, _ success: Bool) -> ()

class NetworkXController: NSObject {
    private let bag = DisposeBag()
    
    static let shared = NetworkXController()
    private override init() {
        super.init()
    }
    
    
    var baseHeaders: HTTPHeaders  {
        set {
            header = newValue
        } get {
            return header
        }
    }
    var header: HTTPHeaders = HTTPHeaders([
            HTTPHeader(name: "Content-Type", value: "application/json"),
            HTTPHeader(name: "Accept", value: "application/json"),
            HTTPHeader(name: "token", value: UserInteractor.getUser()?.token ?? "")
        ])
    
    func request(url: String, parameters: [String: Any] = [:], method: HTTPMethod, headers: HTTPHeaders? = nil, completion: @escaping rxResponse) {
        if let headers = headers{
            headers.forEach {
                baseHeaders.add($0)
            }
        }
        
        print(url)
        print(parameters)
//        print("header is ==> ",headers)
//
        let response = RxAlamofire.requestData(method, url, parameters: parameters, headers: baseHeaders)
        response
            .subscribe(on: MainScheduler.instance)
            .subscribe { (response) in
                print("status code\(response.0.statusCode)", url)
                switch response.0.statusCode {
                case 200:
                    completion(response.1, nil, true)
                case 500:
                    completion(nil, FCError.server, false)
                default:
                    completion(nil, FCError.server, false)
                }
            } onError: { (error) in
                completion(nil, FCError.timeout(message: error.localizedDescription), false)
            } onCompleted: {
                print("complete")
            } onDisposed: {
                print("requeest disposed", url)
            }.disposed(by: bag)
    }
    
}

enum FCError: Error {
    case server
    case empty
    case timeout(message: String)
}

extension FCError: Equatable {
    
    static func == (lhs: FCError, rhs: FCError) -> Bool {
        switch (lhs, rhs) {
        case (.server, server), (empty, empty), (timeout, timeout):
            return true
        default:
            return false
        }
    }
    
}
