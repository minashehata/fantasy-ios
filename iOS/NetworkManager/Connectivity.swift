//
//  Connectivity.swift
//  Template-iOS
//
//  Created by Mina Shehata on 1/3/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//

import Alamofire

extension Notification.Name {
    static let StatusChanged = Notification.Name("StatusChanged")
    static let push_notification = Notification.Name("push_notification")
}

public final class Connectivity: NSObject, Requestable {
    
    static let shared = Connectivity()
    var connected: Bool
    
    private override init() {
        connected = false
        super.init()
    }
    
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager()
    func startNetworkReachabilityObserver() {
        // start listening
        guard let reachabilityManager = reachabilityManager else { return }
        reachabilityManager.startListening(onUpdatePerforming: { [weak self] listener in
            guard let self = self else { return }
            switch listener {
            ///////////////////////////////////
            case .notReachable:
                print("The network is not reachable")
                self.connected = false
                NotificationCenter.default.post(name: .StatusChanged, object: self, userInfo: ["connected": self.connected])
            ///////////////////////////////////
            case .unknown :
                print("It is unknown whether the network is reachable")
                self.connected = false
                NotificationCenter.default.post(name: .StatusChanged, object: self, userInfo: ["connected": self.connected])
            ///////////////////////////////////
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                self.connected = true
                NotificationCenter.default.post(name: .StatusChanged, object: self, userInfo: ["connected": self.connected])
            ///////////////////////////////////
            case .reachable:
                print("The network is reachable over the WWAN connection")
                self.connected = true
                NotificationCenter.default.post(name: .StatusChanged, object: self, userInfo: ["connected": self.connected])
            }
            
        })
    }
    
    func removeNetworkReachability() {
        reachabilityManager?.stopListening()
    }
}
