//
//  NetworkLoader.swift
//  Template-iOS
//
//  Created by Mina Shehata on 1/3/19.
//  Copyright © 2019 Mina Shehata. All rights reserved.
//
import SwiftyJSON
import Alamofire
import AVFoundation
import Kingfisher
import Just

enum HTTPMethodType: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}
public typealias response = (_ data: JSON?, _ success: Bool) -> ()

protocol Requestable: AnyObject {
    func append(request: DataRequest) -> Bool
    func resume() -> Bool
    func cancel() -> Bool
    func suspend() -> Bool
}

final class NetworkLoader: NSObject, Requestable {
    
    let userInteractor = UserInteractor()

    static let shared = NetworkLoader()
    
    var requests: [DataRequest]
    
    var current_request: DataRequest!
    
    private override init() {
        self.requests = []
        super.init()
    }
    
    var get_set_headers: HTTPHeaders {
        set {
            self.headers = newValue
        }
        get {
            return headers
        }
    }
    
    var headers: HTTPHeaders = HTTPHeaders()
    
    var header: [String : String] {
        return [
            "Content-Type" : "application/json",
            "Accept" : "application/json",
            "lang" : Language.currentLanguage().contains("ar") ? "ar" : "en",
            "token": UserInteractor.getUser()?.token ?? ""
           ]
       }
    
    func request(url: String, parameters: [String: Any] = [:], method: Alamofire.HTTPMethod, paramheaders: HTTPHeaders = [:], completion: @escaping response) {
        print(url)
        print(parameters)
        paramheaders.forEach({ head in
            self.headers.add(head)
        })
        self.headers.add(HTTPHeader(name: "Accept", value: "application/json"))
        self.headers.add(HTTPHeader(name: "Content-Type", value: "application/json"))
        self.headers.add(HTTPHeader(name: "token", value: UserInteractor.getUser()?.token ?? ""))
            
        print("header is ==> ",self.get_set_headers)
        let queue = DispatchQueue.global(qos: .userInitiated)
        current_request = AF.request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: self.get_set_headers)
            .validate(statusCode: 200...500)
            .responseJSON(queue: queue, options: []) { (response) in
                guard let status = response.response else { return }
                switch status.statusCode {
                case 200:
                    switch response.result {
                    case .failure(let error):
                        print(error.localizedDescription)
                    case .success(let value):
                        let jsonValue = JSON(value)
                        DispatchQueue.main.async {
                            completion(jsonValue, true)
                        }
                    }
                case 400:
                    switch response.result {
                    case .failure(let error):
                        print(error.localizedDescription)
                    case .success(let value):
                        let jsonValue = JSON(value)
                        print(jsonValue)
                        DispatchQueue.main.async {
                            completion(jsonValue, false)
                        }
                    }
                case 401:
                    DispatchQueue.main.async {
                        UserInteractor.removeUser()
                    }
                default:
                    switch response.result {
                    case .failure(let error):
                        print(error.localizedDescription)
                        DispatchQueue.main.async {
                            UIApplication.topViewController()?.StopLoading()
                            completion(nil, false)
                        }
                    case .success(let value):
                        let jsonValue = JSON(value)
                        print(jsonValue)
                        DispatchQueue.main.async {
                            UserInteractor.removeUser()
                            completion(jsonValue, false)
                        }
                    }
                }
        }
        append(request: current_request)
    }
    
    
    func postRequest(url: String, parameters: [String: Any] = [:], array: [[String: Any]] = [], method: HTTPMethodType, completion: @escaping response) {
        var data: Data?
        if array.count != 0 {
            data = try? JSONSerialization.data(withJSONObject: array, options: [])
        } else {
            data = try? JSONSerialization.data(withJSONObject: parameters, options: [])
        }
        
        switch method {
        case .get:
            Just.get(url, headers: header, asyncCompletionHandler:  { (result) in
                guard let content = result.content else { return }
                let jsonValue = JSON(content)
                //                print(jsonValue)
                switch (result.statusCode ?? 401) {
                case 200:
                    DispatchQueue.main.async {
                        completion(jsonValue, true)
                    }
                case 401:
                    DispatchQueue.main.async {
                        UserInteractor.removeUser()
                    }
                default:
                    DispatchQueue.main.async {
                        completion(jsonValue, false)
                    }
                }
            })
        case .post:
            Just.post(url, headers: header, requestBody: data, asyncCompletionHandler:  { (result) in
                guard let content = result.content else { return }
                let jsonValue = JSON(content)
                print(jsonValue)
                switch result.statusCode! {
                case 200:
                    DispatchQueue.main.async {
                        completion(jsonValue, true)
                    }
                case 401:
                    DispatchQueue.main.async {
                        UserInteractor.removeUser()
                    }
                default:
                    DispatchQueue.main.async {
                        completion(jsonValue, false)
                    }
                }
            })
        case .delete:
            Just.delete(url, headers: header, requestBody: data, asyncCompletionHandler:  { (result) in
                guard let content = result.content else { return }
                let jsonValue = JSON(content)
                print(jsonValue)
                switch result.statusCode! {
                case 200:
                    DispatchQueue.main.async {
                        completion(jsonValue, true)
                    }
                case 401:
                    DispatchQueue.main.async {
                        UserInteractor.removeUser()
                    }
                default:
                    DispatchQueue.main.async {
                        completion(jsonValue, false)
                    }
                }
            })
            
        }
    }
    
    
    
    func requestMultiPart(url: String, parameters: [String: Any] = [:], method: Alamofire.HTTPMethod, completion: @escaping response) {
        
        print(url)
        print(parameters)
        
//        let _ = AF.upload(multipartFormData: { form in
//            for (key, value) in parameters
//            {
//                if key == "logo" {
//                    if let val = value as? Data {
//                        form.append(val, withName: key, fileName: "logo.jpg", mimeType: "image/jpg")
//                    }
//                }
//                else if key == "cover" {
//                    if let val = value as? Data {
//                        form.append(val, withName: key, fileName: "cover.jpg", mimeType: "image/jpg")
//                    }
//                }
//                else if key == "image" {
//                    if let val = value as? Data {
//                        form.append(val, withName: key, fileName: "image.jpg", mimeType: "image/jpg")
//                    }
//                } else if key == "images[0]" {
//                    if let val = value as? Data {
//                        form.append(val, withName: key, fileName: "image.jpg", mimeType: "image/jpg")
//                    }
//                } else {
//                    form.append((value as! String).data(using: .utf8)!, withName: key)
//                }
//            }
//        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: method, headers: headers) { (result: SessionManager.MultipartFormDataEncodingResult) in
//            switch result {
//            case .success(let upload, _, _):
////                if let _ = photo {
//                    upload.uploadProgress(closure: { (progress: Progress) in
//                        let uploadedMB = progress.fractionCompleted * 100
//                        guard let window = UIApplication.shared.keyWindow else { return }
//                        window.subviews.forEach {
//                            if let msLoaderView = $0 as? MSLoadingView {
//                                let percent = "%"
//                                msLoaderView.upload_Label.text = String(format: "Uploading %0.2f %@", arguments: [uploadedMB, percent])
//                            }
//                        }
//                        print("=============\(uploadedMB)******")
//                    })
////                }
//                upload.validate(statusCode: 200...500)
//                upload.responseJSON { response in
//                    guard let status = response.response else { return }
//                    switch status.statusCode {
//                    case 200:
//                        switch response.result {
//                        case .failure(let error):
//                            print(error.localizedDescription)
//                        case .success(let value):
//                            let jsonValue = JSON(value)
//                            print(jsonValue)
//                            DispatchQueue.main.async {
//                                completion(jsonValue, true)
//                            }
//                        }
//                    case 401:
////                        print(JSON(response.result.value))
//                        DispatchQueue.main.async {
//                            self.unauthorized()
//                        }
//                    default:
//                        if response.result.error?._code == NSURLErrorCancelled {
//                            self.resume()
//                            return
//                        }
//                        if response.result.error?._code == NSURLErrorTimedOut {
//                            self.resume()
//                            return
//                        }
//                        switch response.result {
//                        case .failure(let error):
//                            print(error.localizedDescription)
//                            DispatchQueue.main.async {
//                                UIApplication.topViewController()?.StopLoading()
//                                completion(nil, false)
//                            }
//                        case .success(let value):
//                            let jsonValue = JSON(value)
//                            print(jsonValue)
//                            DispatchQueue.main.async {
//                                completion(jsonValue, false)
//                            }
//                        }
//                    }
//                }
//            case .failure(let error):
//                print(error.localizedDescription)
//            }
//        }
    }
    
    
    func downloadImageToImageView(imageView: inout UIImageViewX, url: String) {
        imageView.kf.indicatorType = IndicatorType.activity
        imageView.kf.setImage(with: URL(string: url))
    }
    
    func download_image(from url: String) -> UIImage? {
        let imageView = UIImageView()
        if let url = URL(string: url) {
            imageView.kf.setImage(with: url)
        }
        return imageView.image
    }
    
    func download_image_data(is_video: Bool = false, from url: String, completion: @escaping (Data?, UIImage?) -> ()) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            var image: UIImage?
            let data = try? Data(contentsOf: URL(string: url)!)
            if is_video {
                image = self?.thumbnailImageViewForUrl(url: URL(string: url)!)
            }
            DispatchQueue.main.async {
                completion(data, image)
            }
        }
    }
    func thumbnailImageViewForUrl(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        do {
            let thumbnail = try imageGenerator.copyCGImage(at: CMTime(value: 1, timescale: 60), actualTime: nil)
            return UIImage(cgImage: thumbnail)
        }
        catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
enum MediaType {
    case photo
    case video
}
   
extension Requestable {
    
    @discardableResult func append(request: DataRequest) -> Bool {
        NetworkLoader.shared.requests.append(request)
        return true
    }
    
    @discardableResult func resume() -> Bool {
        NetworkLoader.shared.requests.forEach {
            $0.resume()
        }
        return true
    }
    
    
    @discardableResult func cancel() -> Bool {
        NetworkLoader.shared.requests.forEach {
            $0.cancel()
        }
        return true
    }
    
    @discardableResult func suspend() -> Bool {
        NetworkLoader.shared.requests.forEach {
            $0.suspend()
        }
        return true
    }
}
