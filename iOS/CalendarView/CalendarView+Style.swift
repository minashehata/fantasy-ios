//
//  CalendarView+Style.swift
//  CalendarView
//
//  Created by Vitor Mesquita on 17/01/2018.
//  Copyright © 2018 Karmadust. All rights reserved.
//

import UIKit

extension CalendarView {
    
    public struct Style {
        
        public enum CellShapeOptions {
            case round
            case square
            case bevel(CGFloat)
            var isRound: Bool {
                switch self {
                case .round:
                    return true
                default:
                    return false
                }
            }
        }
        
        public enum FirstWeekdayOptions{
            case sunday
            case monday
        }
        
        //Event
        public static var cellEventColor = UIColor(red: 254.0/255.0, green: 73.0/255.0, blue: 64.0/255.0, alpha: 0.8)
        public static var cellTextEventColor = UIColor.white

        
        //Header
        public static var headerHeight: CGFloat = 80.0
        public static var headerTextColor = UIColor.FDarkGreen
        public static var headerFontName: String = "Helvetica"
        public static var headerFontSize: CGFloat = 20.0

        //Common
        public static var cellShape                 = CellShapeOptions.round
        
        public static var firstWeekday              = FirstWeekdayOptions.monday
        
        //Default Style
        public static var cellColorDefault          = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        public static var cellTextColorDefault      = UIColor.darkGray
        public static var cellBorderColor           = UIColor.clear
        public static var cellBorderWidth           = CGFloat(2.0)
        
        //Today Style
        public static var cellTextColorToday        = UIColor.darkGray
        public static var cellColorToday            = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        //Selected Style
        public static var cellSelectedBorderColor   = UIColor.FDarkGreen
        public static var cellSelectedBorderWidth   = CGFloat(2.0)
        public static var cellSelectedColor         = UIColor.FDarkGreen
        public static var cellSelectedTextColor     = UIColor.white
        
        //Weekend Style
        public static var cellTextColorWeekend      = UIColor.darkGray
        
    }
}
